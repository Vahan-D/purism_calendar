/*
 * This file is generated by gdbus-codegen, do not modify it.
 *
 * The license of this code is the same as for the D-Bus interface description
 * it was derived from. Note that it links to GLib, so must comply with the
 * LGPL linking clauses.
 */

#ifndef __GCAL_SHELL_SEARCH_PROVIDER_GENERATED_H__
#define __GCAL_SHELL_SEARCH_PROVIDER_GENERATED_H__

#include <gio/gio.h>

G_BEGIN_DECLS


/* ------------------------------------------------------------------------ */
/* Declarations for org.gnome.Shell.SearchProvider2 */

#define GCAL_TYPE_SHELL_SEARCH_PROVIDER2 (gcal_shell_search_provider2_get_type ())
#define GCAL_SHELL_SEARCH_PROVIDER2(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2, GcalShellSearchProvider2))
#define GCAL_IS_SHELL_SEARCH_PROVIDER2(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2))
#define GCAL_SHELL_SEARCH_PROVIDER2_GET_IFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2, GcalShellSearchProvider2Iface))

struct _GcalShellSearchProvider2;
typedef struct _GcalShellSearchProvider2 GcalShellSearchProvider2;
typedef struct _GcalShellSearchProvider2Iface GcalShellSearchProvider2Iface;

struct _GcalShellSearchProvider2Iface
{
  GTypeInterface parent_iface;

  gboolean (*handle_activate_result) (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *arg_Result,
    const gchar *const *arg_Terms,
    guint arg_Timestamp);

  gboolean (*handle_get_initial_result_set) (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *const *arg_Terms);

  gboolean (*handle_get_result_metas) (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *const *arg_Results);

  gboolean (*handle_get_subsearch_result_set) (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *const *arg_PreviousResults,
    const gchar *const *arg_Terms);

  gboolean (*handle_launch_search) (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *const *arg_Terms,
    guint arg_Timestamp);

};

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GcalShellSearchProvider2, g_object_unref)
#endif

GType gcal_shell_search_provider2_get_type (void) G_GNUC_CONST;

GDBusInterfaceInfo *gcal_shell_search_provider2_interface_info (void);
guint gcal_shell_search_provider2_override_properties (GObjectClass *klass, guint property_id_begin);


/* D-Bus method call completion functions: */
void gcal_shell_search_provider2_complete_get_initial_result_set (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *const *Results);

void gcal_shell_search_provider2_complete_get_subsearch_result_set (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    const gchar *const *Results);

void gcal_shell_search_provider2_complete_get_result_metas (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation,
    GVariant *Metas);

void gcal_shell_search_provider2_complete_activate_result (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation);

void gcal_shell_search_provider2_complete_launch_search (
    GcalShellSearchProvider2 *object,
    GDBusMethodInvocation *invocation);



/* D-Bus method calls: */
void gcal_shell_search_provider2_call_get_initial_result_set (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_Terms,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean gcal_shell_search_provider2_call_get_initial_result_set_finish (
    GcalShellSearchProvider2 *proxy,
    gchar ***out_Results,
    GAsyncResult *res,
    GError **error);

gboolean gcal_shell_search_provider2_call_get_initial_result_set_sync (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_Terms,
    gchar ***out_Results,
    GCancellable *cancellable,
    GError **error);

void gcal_shell_search_provider2_call_get_subsearch_result_set (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_PreviousResults,
    const gchar *const *arg_Terms,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean gcal_shell_search_provider2_call_get_subsearch_result_set_finish (
    GcalShellSearchProvider2 *proxy,
    gchar ***out_Results,
    GAsyncResult *res,
    GError **error);

gboolean gcal_shell_search_provider2_call_get_subsearch_result_set_sync (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_PreviousResults,
    const gchar *const *arg_Terms,
    gchar ***out_Results,
    GCancellable *cancellable,
    GError **error);

void gcal_shell_search_provider2_call_get_result_metas (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_Results,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean gcal_shell_search_provider2_call_get_result_metas_finish (
    GcalShellSearchProvider2 *proxy,
    GVariant **out_Metas,
    GAsyncResult *res,
    GError **error);

gboolean gcal_shell_search_provider2_call_get_result_metas_sync (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_Results,
    GVariant **out_Metas,
    GCancellable *cancellable,
    GError **error);

void gcal_shell_search_provider2_call_activate_result (
    GcalShellSearchProvider2 *proxy,
    const gchar *arg_Result,
    const gchar *const *arg_Terms,
    guint arg_Timestamp,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean gcal_shell_search_provider2_call_activate_result_finish (
    GcalShellSearchProvider2 *proxy,
    GAsyncResult *res,
    GError **error);

gboolean gcal_shell_search_provider2_call_activate_result_sync (
    GcalShellSearchProvider2 *proxy,
    const gchar *arg_Result,
    const gchar *const *arg_Terms,
    guint arg_Timestamp,
    GCancellable *cancellable,
    GError **error);

void gcal_shell_search_provider2_call_launch_search (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_Terms,
    guint arg_Timestamp,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean gcal_shell_search_provider2_call_launch_search_finish (
    GcalShellSearchProvider2 *proxy,
    GAsyncResult *res,
    GError **error);

gboolean gcal_shell_search_provider2_call_launch_search_sync (
    GcalShellSearchProvider2 *proxy,
    const gchar *const *arg_Terms,
    guint arg_Timestamp,
    GCancellable *cancellable,
    GError **error);



/* ---- */

#define GCAL_TYPE_SHELL_SEARCH_PROVIDER2_PROXY (gcal_shell_search_provider2_proxy_get_type ())
#define GCAL_SHELL_SEARCH_PROVIDER2_PROXY(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_PROXY, GcalShellSearchProvider2Proxy))
#define GCAL_SHELL_SEARCH_PROVIDER2_PROXY_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_PROXY, GcalShellSearchProvider2ProxyClass))
#define GCAL_SHELL_SEARCH_PROVIDER2_PROXY_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_PROXY, GcalShellSearchProvider2ProxyClass))
#define GCAL_IS_SHELL_SEARCH_PROVIDER2_PROXY(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_PROXY))
#define GCAL_IS_SHELL_SEARCH_PROVIDER2_PROXY_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_PROXY))

typedef struct _GcalShellSearchProvider2Proxy GcalShellSearchProvider2Proxy;
typedef struct _GcalShellSearchProvider2ProxyClass GcalShellSearchProvider2ProxyClass;
typedef struct _GcalShellSearchProvider2ProxyPrivate GcalShellSearchProvider2ProxyPrivate;

struct _GcalShellSearchProvider2Proxy
{
  /*< private >*/
  GDBusProxy parent_instance;
  GcalShellSearchProvider2ProxyPrivate *priv;
};

struct _GcalShellSearchProvider2ProxyClass
{
  GDBusProxyClass parent_class;
};

GType gcal_shell_search_provider2_proxy_get_type (void) G_GNUC_CONST;

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GcalShellSearchProvider2Proxy, g_object_unref)
#endif

void gcal_shell_search_provider2_proxy_new (
    GDBusConnection     *connection,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GAsyncReadyCallback  callback,
    gpointer             user_data);
GcalShellSearchProvider2 *gcal_shell_search_provider2_proxy_new_finish (
    GAsyncResult        *res,
    GError             **error);
GcalShellSearchProvider2 *gcal_shell_search_provider2_proxy_new_sync (
    GDBusConnection     *connection,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GError             **error);

void gcal_shell_search_provider2_proxy_new_for_bus (
    GBusType             bus_type,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GAsyncReadyCallback  callback,
    gpointer             user_data);
GcalShellSearchProvider2 *gcal_shell_search_provider2_proxy_new_for_bus_finish (
    GAsyncResult        *res,
    GError             **error);
GcalShellSearchProvider2 *gcal_shell_search_provider2_proxy_new_for_bus_sync (
    GBusType             bus_type,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GError             **error);


/* ---- */

#define GCAL_TYPE_SHELL_SEARCH_PROVIDER2_SKELETON (gcal_shell_search_provider2_skeleton_get_type ())
#define GCAL_SHELL_SEARCH_PROVIDER2_SKELETON(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_SKELETON, GcalShellSearchProvider2Skeleton))
#define GCAL_SHELL_SEARCH_PROVIDER2_SKELETON_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_SKELETON, GcalShellSearchProvider2SkeletonClass))
#define GCAL_SHELL_SEARCH_PROVIDER2_SKELETON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_SKELETON, GcalShellSearchProvider2SkeletonClass))
#define GCAL_IS_SHELL_SEARCH_PROVIDER2_SKELETON(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_SKELETON))
#define GCAL_IS_SHELL_SEARCH_PROVIDER2_SKELETON_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GCAL_TYPE_SHELL_SEARCH_PROVIDER2_SKELETON))

typedef struct _GcalShellSearchProvider2Skeleton GcalShellSearchProvider2Skeleton;
typedef struct _GcalShellSearchProvider2SkeletonClass GcalShellSearchProvider2SkeletonClass;
typedef struct _GcalShellSearchProvider2SkeletonPrivate GcalShellSearchProvider2SkeletonPrivate;

struct _GcalShellSearchProvider2Skeleton
{
  /*< private >*/
  GDBusInterfaceSkeleton parent_instance;
  GcalShellSearchProvider2SkeletonPrivate *priv;
};

struct _GcalShellSearchProvider2SkeletonClass
{
  GDBusInterfaceSkeletonClass parent_class;
};

GType gcal_shell_search_provider2_skeleton_get_type (void) G_GNUC_CONST;

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GcalShellSearchProvider2Skeleton, g_object_unref)
#endif

GcalShellSearchProvider2 *gcal_shell_search_provider2_skeleton_new (void);


G_END_DECLS

#endif /* __GCAL_SHELL_SEARCH_PROVIDER_GENERATED_H__ */
