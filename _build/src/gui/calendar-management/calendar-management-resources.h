#ifndef __RESOURCE_calendar_management_H__
#define __RESOURCE_calendar_management_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *calendar_management_get_resource (void);
#endif
