#ifndef __RESOURCE_event_editor_H__
#define __RESOURCE_event_editor_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *event_editor_get_resource (void);
#endif
