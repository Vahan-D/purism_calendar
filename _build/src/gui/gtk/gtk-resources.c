#include <gio/gio.h>

#if defined (__ELF__) && ( __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 6))
# define SECTION __attribute__ ((section (".gresource.gtk"), aligned (8)))
#else
# define SECTION
#endif

#ifdef _MSC_VER
static const SECTION union { const guint8 data[821]; const double alignment; void * const ptr;}  gtk_resource_data = { {
  0107, 0126, 0141, 0162, 0151, 0141, 0156, 0164, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 
  0030, 0000, 0000, 0000, 0310, 0000, 0000, 0000, 0000, 0000, 0000, 0050, 0006, 0000, 0000, 0000, 
  0000, 0000, 0000, 0000, 0001, 0000, 0000, 0000, 0002, 0000, 0000, 0000, 0005, 0000, 0000, 0000, 
  0005, 0000, 0000, 0000, 0005, 0000, 0000, 0000, 0322, 0055, 0374, 0355, 0002, 0000, 0000, 0000, 
  0310, 0000, 0000, 0000, 0017, 0000, 0166, 0000, 0330, 0000, 0000, 0000, 0375, 0002, 0000, 0000, 
  0113, 0120, 0220, 0013, 0004, 0000, 0000, 0000, 0375, 0002, 0000, 0000, 0004, 0000, 0114, 0000, 
  0004, 0003, 0000, 0000, 0010, 0003, 0000, 0000, 0016, 0107, 0266, 0230, 0005, 0000, 0000, 0000, 
  0010, 0003, 0000, 0000, 0004, 0000, 0114, 0000, 0014, 0003, 0000, 0000, 0020, 0003, 0000, 0000, 
  0260, 0267, 0044, 0060, 0001, 0000, 0000, 0000, 0020, 0003, 0000, 0000, 0006, 0000, 0114, 0000, 
  0030, 0003, 0000, 0000, 0034, 0003, 0000, 0000, 0324, 0265, 0002, 0000, 0377, 0377, 0377, 0377, 
  0034, 0003, 0000, 0000, 0001, 0000, 0114, 0000, 0040, 0003, 0000, 0000, 0044, 0003, 0000, 0000, 
  0031, 0154, 0341, 0353, 0003, 0000, 0000, 0000, 0044, 0003, 0000, 0000, 0011, 0000, 0114, 0000, 
  0060, 0003, 0000, 0000, 0064, 0003, 0000, 0000, 0150, 0145, 0154, 0160, 0055, 0157, 0166, 0145, 
  0162, 0154, 0141, 0171, 0056, 0165, 0151, 0000, 0263, 0026, 0000, 0000, 0001, 0000, 0000, 0000, 
  0170, 0332, 0355, 0230, 0113, 0217, 0323, 0060, 0020, 0200, 0357, 0373, 0053, 0054, 0037, 0366, 
  0126, 0112, 0341, 0000, 0322, 0046, 0331, 0003, 0250, 0345, 0000, 0125, 0241, 0135, 0126, 0234, 
  0126, 0256, 0063, 0115, 0114, 0035, 0073, 0330, 0116, 0322, 0376, 0173, 0034, 0245, 0025, 0171, 
  0165, 0037, 0305, 0102, 0242, 0312, 0055, 0216, 0347, 0341, 0371, 0074, 0361, 0214, 0343, 0335, 
  0356, 0022, 0216, 0162, 0120, 0232, 0111, 0341, 0343, 0311, 0253, 0327, 0030, 0201, 0240, 0062, 
  0144, 0042, 0362, 0361, 0335, 0152, 0072, 0172, 0217, 0157, 0203, 0053, 0217, 0011, 0003, 0152, 
  0103, 0050, 0004, 0127, 0010, 0171, 0162, 0375, 0023, 0250, 0101, 0224, 0023, 0255, 0175, 0074, 
  0063, 0333, 0145, 0054, 0225, 0241, 0231, 0321, 0367, 0114, 0204, 0262, 0300, 0210, 0205, 0076, 
  0216, 0201, 0247, 0017, 0322, 0332, 0346, 0144, 0217, 0113, 0075, 0253, 0231, 0052, 0231, 0202, 
  0062, 0173, 0044, 0110, 0002, 0076, 0116, 0144, 0110, 0070, 0016, 0126, 0052, 0003, 0157, 0174, 
  0234, 0073, 0210, 0322, 0230, 0361, 0260, 0172, 0176, 0324, 0345, 0322, 0276, 0267, 0213, 0307, 
  0107, 0321, 0256, 0227, 0234, 0151, 0266, 0346, 0320, 0357, 0247, 0117, 0101, 0127, 0046, 0107, 
  0345, 0010, 0007, 0372, 0350, 0351, 0071, 0252, 0011, 0331, 0215, 0142, 0140, 0121, 0154, 0160, 
  0060, 0171, 0327, 0253, 0321, 0010, 0354, 0211, 0340, 0146, 0112, 0146, 0051, 0256, 0013, 0237, 
  0021, 0136, 0237, 0222, 0141, 0306, 0252, 0040, 0243, 0210, 0320, 0234, 0030, 0142, 0015, 0370, 
  0170, 0017, 0032, 0043, 0052, 0355, 0136, 0357, 0214, 0245, 0160, 0130, 0004, 0052, 0252, 0115, 
  0015, 0146, 0040, 0100, 0021, 0176, 0322, 0105, 0047, 0260, 0247, 0166, 0356, 0360, 0200, 0333, 
  0072, 0147, 0306, 0350, 0052, 0316, 0071, 0024, 0010, 0162, 0020, 0346, 0045, 0216, 0010, 0245, 
  0300, 0055, 0036, 0043, 0025, 0016, 0256, 0271, 0271, 0131, 0050, 0226, 0020, 0265, 0277, 0216, 
  0314, 0315, 0374, 0264, 0041, 0157, 0134, 0361, 0151, 0241, 0034, 0367, 0260, 0274, 0030, 0276, 
  0037, 0270, 0324, 0160, 0030, 0271, 0102, 0374, 0165, 0100, 0134, 0107, 0274, 0004, 0242, 0150, 
  0354, 0012, 0356, 0164, 0200, 0333, 0200, 0033, 0313, 0002, 0225, 0325, 0355, 0154, 0276, 0323, 
  0311, 0100, 0264, 0105, 0364, 0164, 0211, 0075, 0047, 0143, 0177, 0145, 0240, 0313, 0022, 0216, 
  0332, 0231, 0354, 0002, 0174, 0127, 0260, 0043, 0364, 0377, 0026, 0371, 0071, 0311, 0131, 0104, 
  0112, 0166, 0027, 0137, 0347, 0147, 0022, 0255, 0011, 0335, 0376, 0125, 0316, 0021, 0156, 0312, 
  0274, 0372, 0014, 0033, 0203, 0026, 0044, 0202, 0207, 0273, 0164, 0370, 0264, 0133, 0220, 0067, 
  0122, 0025, 0104, 0205, 0056, 0070, 0177, 0053, 0073, 0353, 0012, 0364, 0107, 0131, 0210, 0001, 
  0165, 0247, 0056, 0031, 0173, 0245, 0332, 0273, 0100, 0135, 0362, 0155, 0037, 0237, 0053, 0364, 
  0111, 0046, 0060, 0120, 0157, 0336, 0026, 0166, 0006, 0345, 0014, 0234, 0265, 0262, 0103, 0166, 
  0367, 0162, 0136, 0050, 0310, 0231, 0314, 0264, 0173, 0326, 0116, 0216, 0354, 0213, 0156, 0012, 
  0276, 0077, 0202, 0374, 0142, 0022, 0354, 0036, 0140, 0353, 0064, 0271, 0206, 0046, 0277, 0301, 
  0367, 0213, 0235, 0211, 0235, 0002, 0176, 0063, 0000, 0256, 0003, 0376, 0141, 0057, 0375, 0116, 
  0371, 0276, 0375, 0027, 0347, 0142, 0123, 0240, 0066, 0371, 0147, 0302, 0033, 0327, 0376, 0077, 
  0377, 0006, 0110, 0243, 0304, 0007, 0000, 0050, 0165, 0165, 0141, 0171, 0051, 0157, 0162, 0147, 
  0057, 0000, 0000, 0000, 0003, 0000, 0000, 0000, 0147, 0164, 0153, 0057, 0000, 0000, 0000, 0000, 
  0147, 0156, 0157, 0155, 0145, 0057, 0000, 0000, 0005, 0000, 0000, 0000, 0057, 0000, 0000, 0000, 
  0001, 0000, 0000, 0000, 0143, 0141, 0154, 0145, 0156, 0144, 0141, 0162, 0057, 0000, 0000, 0000, 
  0002, 0000, 0000, 0000
} };
#else /* _MSC_VER */
static const SECTION union { const guint8 data[821]; const double alignment; void * const ptr;}  gtk_resource_data = {
  "\107\126\141\162\151\141\156\164\000\000\000\000\000\000\000\000"
  "\030\000\000\000\310\000\000\000\000\000\000\050\006\000\000\000"
  "\000\000\000\000\001\000\000\000\002\000\000\000\005\000\000\000"
  "\005\000\000\000\005\000\000\000\322\055\374\355\002\000\000\000"
  "\310\000\000\000\017\000\166\000\330\000\000\000\375\002\000\000"
  "\113\120\220\013\004\000\000\000\375\002\000\000\004\000\114\000"
  "\004\003\000\000\010\003\000\000\016\107\266\230\005\000\000\000"
  "\010\003\000\000\004\000\114\000\014\003\000\000\020\003\000\000"
  "\260\267\044\060\001\000\000\000\020\003\000\000\006\000\114\000"
  "\030\003\000\000\034\003\000\000\324\265\002\000\377\377\377\377"
  "\034\003\000\000\001\000\114\000\040\003\000\000\044\003\000\000"
  "\031\154\341\353\003\000\000\000\044\003\000\000\011\000\114\000"
  "\060\003\000\000\064\003\000\000\150\145\154\160\055\157\166\145"
  "\162\154\141\171\056\165\151\000\263\026\000\000\001\000\000\000"
  "\170\332\355\230\113\217\323\060\020\200\357\373\053\054\037\366"
  "\126\112\341\000\322\046\331\003\250\345\000\125\241\135\126\234"
  "\126\256\063\115\114\035\073\330\116\322\376\173\034\245\025\171"
  "\165\037\305\102\242\312\055\216\347\341\371\074\361\214\343\335"
  "\356\022\216\162\120\232\111\341\343\311\253\327\030\201\240\062"
  "\144\042\362\361\335\152\072\172\217\157\203\053\217\011\003\152"
  "\103\050\004\127\010\171\162\375\023\250\101\224\023\255\175\074"
  "\063\333\145\054\225\241\231\321\367\114\204\262\300\210\205\076"
  "\216\201\247\017\322\332\346\144\217\113\075\253\231\052\231\202"
  "\062\173\044\110\002\076\116\144\110\070\016\126\052\003\157\174"
  "\234\073\210\322\230\361\260\172\176\324\345\322\276\267\213\307"
  "\107\321\256\227\234\151\266\346\320\357\247\117\101\127\046\107"
  "\345\010\007\372\350\351\071\252\011\331\215\142\140\121\154\160"
  "\060\171\327\253\321\010\354\211\340\146\112\146\051\256\013\237"
  "\021\136\237\222\141\306\252\040\243\210\320\234\030\142\015\370"
  "\170\017\032\043\052\355\136\357\214\245\160\130\004\052\252\115"
  "\015\146\040\100\021\176\322\105\047\260\247\166\356\360\200\333"
  "\072\147\306\350\052\316\071\024\010\162\020\346\045\216\010\245"
  "\300\055\036\043\025\016\256\271\271\131\050\226\020\265\277\216"
  "\314\315\374\264\041\157\134\361\151\241\034\367\260\274\030\276"
  "\037\270\324\160\030\271\102\374\165\100\134\107\274\004\242\150"
  "\354\012\356\164\200\333\200\033\313\002\225\325\355\154\276\323"
  "\311\100\264\105\364\164\211\075\047\143\177\145\240\313\022\216"
  "\332\231\354\002\174\127\260\043\364\377\026\371\071\311\131\104"
  "\112\166\027\137\347\147\022\255\011\335\376\125\316\021\156\312"
  "\274\372\014\033\203\026\044\202\207\273\164\370\264\133\220\067"
  "\122\025\104\205\056\070\177\053\073\353\012\364\107\131\210\001"
  "\165\247\056\031\173\245\332\273\100\135\362\155\037\237\053\364"
  "\111\046\060\120\157\336\026\166\006\345\014\234\265\262\103\166"
  "\367\162\136\050\310\231\314\264\173\326\116\216\354\213\156\012"
  "\276\077\202\374\142\022\354\036\140\353\064\271\206\046\277\301"
  "\367\213\235\211\235\002\176\063\000\256\003\376\141\057\375\116"
  "\371\276\375\027\347\142\123\240\066\371\147\302\033\327\376\077"
  "\377\006\110\243\304\007\000\050\165\165\141\171\051\157\162\147"
  "\057\000\000\000\003\000\000\000\147\164\153\057\000\000\000\000"
  "\147\156\157\155\145\057\000\000\005\000\000\000\057\000\000\000"
  "\001\000\000\000\143\141\154\145\156\144\141\162\057\000\000\000"
  "\002\000\000\000" };
#endif /* !_MSC_VER */

static GStaticResource static_resource = { gtk_resource_data.data, sizeof (gtk_resource_data.data) - 1 /* nul terminator */, NULL, NULL, NULL };

G_GNUC_INTERNAL
GResource *gtk_get_resource (void);
GResource *gtk_get_resource (void)
{
  return g_static_resource_get_resource (&static_resource);
}
/*
  If G_HAS_CONSTRUCTORS is true then the compiler support *both* constructors and
  destructors, in a usable way, including e.g. on library unload. If not you're on
  your own.

  Some compilers need #pragma to handle this, which does not work with macros,
  so the way you need to use this is (for constructors):

  #ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
  #pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(my_constructor)
  #endif
  G_DEFINE_CONSTRUCTOR(my_constructor)
  static void my_constructor(void) {
   ...
  }

*/

#ifndef __GTK_DOC_IGNORE__

#if  __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR(_func) static void __attribute__((constructor)) _func (void);
#define G_DEFINE_DESTRUCTOR(_func) static void __attribute__((destructor)) _func (void);

#elif defined (_MSC_VER) && (_MSC_VER >= 1500)
/* Visual studio 2008 and later has _Pragma */

#include <stdlib.h>

#define G_HAS_CONSTRUCTORS 1

/* We do some weird things to avoid the constructors being optimized
 * away on VS2015 if WholeProgramOptimization is enabled. First we
 * make a reference to the array from the wrapper to make sure its
 * references. Then we use a pragma to make sure the wrapper function
 * symbol is always included at the link stage. Also, the symbols
 * need to be extern (but not dllexport), even though they are not
 * really used from another object file.
 */

/* We need to account for differences between the mangling of symbols
 * for x86 and x64/ARM/ARM64 programs, as symbols on x86 are prefixed
 * with an underscore but symbols on x64/ARM/ARM64 are not.
 */
#ifdef _M_IX86
#define G_MSVC_SYMBOL_PREFIX "_"
#else
#define G_MSVC_SYMBOL_PREFIX ""
#endif

#define G_DEFINE_CONSTRUCTOR(_func) G_MSVC_CTOR (_func, G_MSVC_SYMBOL_PREFIX)
#define G_DEFINE_DESTRUCTOR(_func) G_MSVC_DTOR (_func, G_MSVC_SYMBOL_PREFIX)

#define G_MSVC_CTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _wrapper(void) { _func(); g_slist_find (NULL,  _array ## _func); return 0; } \
  __pragma(comment(linker,"/include:" _sym_prefix # _func "_wrapper")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _wrapper;

#define G_MSVC_DTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _constructor(void) { atexit (_func); g_slist_find (NULL,  _array ## _func); return 0; } \
   __pragma(comment(linker,"/include:" _sym_prefix # _func "_constructor")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _constructor;

#elif defined (_MSC_VER)

#define G_HAS_CONSTRUCTORS 1

/* Pre Visual studio 2008 must use #pragma section */
#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _wrapper(void) { _func(); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (*p)(void) = _func ## _wrapper;

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;

#elif defined(__SUNPRO_C)

/* This is not tested, but i believe it should work, based on:
 * http://opensource.apple.com/source/OpenSSL098/OpenSSL098-35/src/fips/fips_premain.c
 */

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  init(_func)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void);

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  fini(_func)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void);

#else

/* constructors not supported for this compiler */

#endif

#endif /* __GTK_DOC_IGNORE__ */

#ifdef G_HAS_CONSTRUCTORS

#ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(resource_constructor)
#endif
G_DEFINE_CONSTRUCTOR(resource_constructor)
#ifdef G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(resource_destructor)
#endif
G_DEFINE_DESTRUCTOR(resource_destructor)

#else
#warning "Constructor not supported on this compiler, linking in resources will not work"
#endif

static void resource_constructor (void)
{
  g_static_resource_init (&static_resource);
}

static void resource_destructor (void)
{
  g_static_resource_fini (&static_resource);
}
