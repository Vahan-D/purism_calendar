#ifndef __RESOURCE_gtk_H__
#define __RESOURCE_gtk_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *gtk_get_resource (void);
#endif
