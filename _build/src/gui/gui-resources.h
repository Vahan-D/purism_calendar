#ifndef __RESOURCE_gui_H__
#define __RESOURCE_gui_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *gui_get_resource (void);
#endif
