#ifndef __RESOURCE_icons_H__
#define __RESOURCE_icons_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *icons_get_resource (void);
#endif
