#ifndef __RESOURCE_views_H__
#define __RESOURCE_views_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *views_get_resource (void);
#endif
