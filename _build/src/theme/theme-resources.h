#ifndef __RESOURCE_theme_H__
#define __RESOURCE_theme_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *theme_get_resource (void);
#endif
