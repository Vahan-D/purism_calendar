# Lithuanian translation for gnome-calendar.
# Copyright (C) 2015 gnome-calendar's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-calendar package.
# brennus <jonas.ska@gmail.com>, 2015.
# Jonas Skarulskis <jonas.ska@gmail.com>, 2015.
# Aurimas Černius <aurisc4@gmail.com>, 2015-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-calendar master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-calendar/issues\n"
"POT-Creation-Date: 2020-11-07 10:58+0000\n"
"PO-Revision-Date: 2021-02-21 21:34+0200\n"
"Last-Translator: Aurimas Černius <aurisc4@gmail.com>\n"
"Language-Team: Lietuvių <gnome-lt@lists.akl.lt>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n"
"%100<10 || n%100>=20) ? 1 : 2)\n"
"X-Generator: Gtranslator 3.38.0\n"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:7
#: data/org.gnome.Calendar.desktop.in.in:3 src/main.c:35
#: src/gui/gcal-application.c:277 src/gui/gcal-quick-add-popover.ui:187
#: src/gui/gcal-window.ui:170
msgid "Calendar"
msgstr "Kalendorius"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:8
msgid "Calendar for GNOME"
msgstr "GNOME kalendorius"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:11
msgid ""
"GNOME Calendar is a simple and beautiful calendar application designed to "
"perfectly fit the GNOME desktop. By reusing the components which the GNOME "
"desktop is built on, Calendar nicely integrates with the GNOME ecosystem."
msgstr ""
"Gnome kalendorius yra paprasta ir patraukli kalendoriaus programa, sukurtas "
"specialiai GNOME darbalaukiui. Panaudojant kitus GNOME komponentus "
"kalendorius patogiai integruotas į GNOME ekosistemą."

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:16
msgid ""
"We aim to find the perfect balance between nicely crafted features and user-"
"centred usability. No excess, nothing missing. You’ll feel comfortable using "
"Calendar, like you’ve been using it for ages!"
msgstr ""
"Mūsų tikslas – tobula gerai paruoštų funkcijų ir į vartotoją orientuotos "
"sąsajos harmonija. Nieko per daug, nieko trūkstamo. Naudodami kalendorių "
"jausitės taip, lyg tai jau darėte metų metus!"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:27
msgid "Week view"
msgstr "Savaitės rodinys"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:31
msgid "Year view"
msgstr "Metų rodinys"

#: data/org.gnome.Calendar.desktop.in.in:4
msgid "Access and manage your calendars"
msgstr "Prieikite ir tvarkykite savo kalendorius"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calendar.desktop.in.in:13
msgid "Calendar;Event;Reminder;"
msgstr "Kalendorius;Įvykis;Priminimas;"

#: data/org.gnome.calendar.gschema.xml.in:6
msgid "Window maximized"
msgstr "Langas išdidintas"

#: data/org.gnome.calendar.gschema.xml.in:7
msgid "Window maximized state"
msgstr "Lango išdidinimo būsena"

#: data/org.gnome.calendar.gschema.xml.in:11
msgid "Window size"
msgstr "Lango dydis"

#: data/org.gnome.calendar.gschema.xml.in:12
msgid "Window size (width and height)."
msgstr "Lango dydis (plotis ir aukštis)."

#: data/org.gnome.calendar.gschema.xml.in:16
msgid "Window position"
msgstr "Lango padėtis"

#: data/org.gnome.calendar.gschema.xml.in:17
msgid "Window position (x and y)."
msgstr "Lango padėtis (x ir y)."

#: data/org.gnome.calendar.gschema.xml.in:21
msgid "Type of the active view"
msgstr "Įjungto vaizdo tipas"

#: data/org.gnome.calendar.gschema.xml.in:22
msgid "Type of the active window view, default value is: monthly view"
msgstr "Įjungto vaizdo tipas, numatyta reikšmė yra mėnesio vaizdas"

#: data/org.gnome.calendar.gschema.xml.in:26
msgid "Weather Service Configuration"
msgstr "Orų tarnybos konfigūracija"

#: data/org.gnome.calendar.gschema.xml.in:27
msgid ""
"Whether weather reports are shown, automatic locations are used and a "
"location-name"
msgstr ""
"Ar rodomos orų prognozės, naudojamos automatinės vietos bei vietos "
"pavadinimas"

#: data/org.gnome.calendar.gschema.xml.in:31
msgid "Follow system night light"
msgstr "Sekti sistemos naktinį apšvietimą"

#: data/org.gnome.calendar.gschema.xml.in:32
msgid "Use GNOME night light setting to activate night-mode."
msgstr ""
"Naudoti GNOME naktinio apšvietimo nustatymą naktinei veiksenai įjungti."

#. Translators: %1$s is the start date and %2$s is the end date.
#: src/core/gcal-event.c:1882
#, c-format
msgid "%1$s — %2$s"
msgstr "%1$s — %2$s"

#.
#. * Translators: %1$s is the start date, %2$s is the start time,
#. * %3$s is the end date, and %4$s is the end time.
#.
#: src/core/gcal-event.c:1890
#, c-format
msgid "%1$s %2$s — %3$s %4$s"
msgstr "%1$s %2$s — %3$s %4$s"

#. Translators: %1$s is a date, %2$s is the start hour, and %3$s is the end hour
#. Translators: %1$s is the event name, %2$s is the start hour, and %3$s is the end hour
#: src/core/gcal-event.c:1906 src/gui/gcal-quick-add-popover.c:472
#, c-format
msgid "%1$s, %2$s – %3$s"
msgstr "%1$s, %2$s – %3$s"

#: src/gui/calendar-management/gcal-calendar-management-dialog.ui:42
msgid "Calendar Settings"
msgstr "Kalendoriaus nustatymai"

#: src/gui/calendar-management/gcal-calendars-page.c:359
msgid "Manage Calendars"
msgstr "Tvarkyti kalendorius"

#. Update notification label
#: src/gui/calendar-management/gcal-calendars-page.c:389
#, c-format
msgid "Calendar <b>%s</b> removed"
msgstr "Kalendorius <b>%s</b> pašalintas"

#: src/gui/calendar-management/gcal-calendars-page.ui:31
#: src/gui/gcal-window.c:672 src/gui/gcal-window.c:676
msgid "Undo"
msgstr "Atšaukti"

#: src/gui/calendar-management/gcal-calendars-page.ui:97
msgid "Add Calendar…"
msgstr "Pridėti kalendorių…"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:18
msgid "Account"
msgstr "Paskyra"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:43
msgid "Settings"
msgstr "Nustatymai"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:64
#: src/gui/event-editor/gcal-summary-section.ui:37
msgid "Location"
msgstr "Vieta"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:93
msgid "Calendar name"
msgstr "Kalendoriaus pavadinimas"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:121
#: src/gui/calendar-management/gcal-new-calendar-page.ui:65
msgid "Color"
msgstr "Spalva"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:155
msgid "Display calendar"
msgstr "Rodyti kalendorių"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:175
msgid "Add new events to this calendar by default"
msgstr "Numatytai pridėti naujus įvykius į šį kalendorių"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:190
msgid "Remove Calendar"
msgstr "Pašalinti kalendorių"

#: src/gui/calendar-management/gcal-new-calendar-page.c:505
msgid "New Calendar"
msgstr "Naujas kalendorius"

#: src/gui/calendar-management/gcal-new-calendar-page.c:667
msgid "Calendar files"
msgstr "Kalendoriaus failai"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:33
#: src/gui/calendar-management/gcal-new-calendar-page.ui:41
msgid "Calendar Name"
msgstr "Kalendoriaus pavadinimas"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:86
msgid "Import a Calendar"
msgstr "Importuoti kalendorių"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:105
msgid ""
"Alternatively, enter the web address of an online calendar you want to "
"import, or open a supported calendar file."
msgstr ""
"Taip pat galite įvesti importuojamo internetinio kalendoriaus internetinį "
"adresą arba atverti palaikomą kalendoriaus failą."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:137
msgid "Open a File"
msgstr "Atverti failą"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:158
msgid "Calendars"
msgstr "Kalendoriai"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:204
msgid ""
"If the calendar belongs to one of your online accounts, you can add it "
"through the <a href=\"GOA\">online account settings</a>."
msgstr ""
"Jei kalendorius priklauso vienai iš jūsų internetinių paskyrų, ją galite "
"pridėti per <a href=\"GOA\">internetinių paskyrų nustatymus</a>."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:240
msgid "User"
msgstr "Naudotojas"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:254
msgid "Password"
msgstr "Slaptažodis"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:302
#: src/gui/calendar-management/gcal-new-calendar-page.ui:341
#: src/gui/event-editor/gcal-event-editor-dialog.ui:17
msgid "Cancel"
msgstr "Atsisakyti"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:311
msgid "Connect"
msgstr "Prisijungti"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:348
#: src/gui/gcal-quick-add-popover.ui:130
msgid "Add"
msgstr "Pridėti"

#: src/gui/event-editor/gcal-alarm-row.c:85
#, c-format
msgid "%1$u day, %2$u hour, and %3$u minute before"
msgid_plural "%1$u day, %2$u hour, and %3$u minutes before"
msgstr[0] "Prieš %1$u dieną, %2$u valandą ir %3$u minutę"
msgstr[1] "Prieš %1$u dieną, %2$u valandą ir %3$u minutes"
msgstr[2] "Prieš %1$u dieną, %2$u valandą ir %3$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:89
#, c-format
msgid "%1$u day, %2$u hours, and %3$u minute before"
msgid_plural "%1$u day, %2$u hours, and %3$u minutes before"
msgstr[0] "Prieš %1$u dieną, %2$u valandas ir %3$u minutę"
msgstr[1] "Prieš %1$u dieną, %2$u valandas ir %3$u minutes"
msgstr[2] "Prieš %1$u dieną, %2$u valandas ir %3$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:95
#, c-format
msgid "%1$u days, %2$u hour, and %3$u minute before"
msgid_plural "%1$u days, %2$u hour, and %3$u minutes before"
msgstr[0] "Prieš %1$u dieną, %2$u valandą ir %3$u minutę"
msgstr[1] "Prieš %1$u dienas, %2$u valandą ir %3$u minutę"
msgstr[2] "Prieš %1$u dienų, %2$u valandą ir %3$u minutę"

#: src/gui/event-editor/gcal-alarm-row.c:99
#, c-format
msgid "%1$u days, %2$u hours, and %3$u minute before"
msgid_plural "%1$u days, %2$u hours, and %3$u minutes before"
msgstr[0] "Prieš %1$u dienas, %2$u valandas ir %3$u minutę"
msgstr[1] "Prieš %1$u dienas, %2$u valandas ir %3$u minutes"
msgstr[2] "Prieš %1$u dienas, %2$u valandas ir %3$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:114
#, c-format
msgid "%1$u day and %2$u hour before"
msgid_plural "%1$u day and %2$u hours before"
msgstr[0] "Prieš %1$u dieną ir %2$u valandą"
msgstr[1] "Prieš %1$u dieną ir %2$u valandas"
msgstr[2] "Prieš %1$u dieną ir %2$u valandų"

#: src/gui/event-editor/gcal-alarm-row.c:118
#, c-format
msgid "%1$u days and %2$u hour before"
msgid_plural "%1$u days and %2$u hours before"
msgstr[0] "Prieš %1$u dienas ir %2$u valandą"
msgstr[1] "Prieš %1$u dienas ir %2$u valandas"
msgstr[2] "Prieš %1$u dienas ir %2$u valandų"

#: src/gui/event-editor/gcal-alarm-row.c:134
#, c-format
msgid "%1$u day and %2$u minute before"
msgid_plural "%1$u day and %2$u minutes before"
msgstr[0] "Prieš %1$u dieną ir %2$u minutę"
msgstr[1] "Prieš %1$u dieną ir %2$u minutes"
msgstr[2] "Prieš %1$u dieną ir %2$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:138
#, c-format
msgid "%1$u days and %2$u minute before"
msgid_plural "%1$u days and %2$u minutes before"
msgstr[0] "Prieš %1$u dienas ir %2$u minutę"
msgstr[1] "Prieš %1$u dienas ir %2$u minutes"
msgstr[2] "Prieš %1$u dienas ir %2$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:148
#, c-format
msgid "%1$u day before"
msgid_plural "%1$u days before"
msgstr[0] "Prieš %1$u dieną"
msgstr[1] "Prieš %1$u dienas"
msgstr[2] "Prieš %1$u dienų"

#: src/gui/event-editor/gcal-alarm-row.c:166
#, c-format
msgid "%1$u hour and %2$u minute before"
msgid_plural "%1$u hour and %2$u minutes before"
msgstr[0] "Prieš %1$u valandą ir %2$u minutę"
msgstr[1] "Prieš %1$u valandą ir %2$u minutes"
msgstr[2] "Prieš %1$u valandą ir %2$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:170
#, c-format
msgid "%1$u hours and %2$u minute before"
msgid_plural "%1$u hours and %2$u minutes before"
msgstr[0] "Prieš %1$u valandas ir %2$u minutę"
msgstr[1] "Prieš %1$u valandas ir %2$u minutes"
msgstr[2] "Prieš %1$u valandas ir %2$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:180
#, c-format
msgid "%1$u hour before"
msgid_plural "%1$u hours before"
msgstr[0] "Prieš %1$u valandą"
msgstr[1] "Prieš %1$u valandas"
msgstr[2] "Prieš %1$u valandų"

#: src/gui/event-editor/gcal-alarm-row.c:192
#, c-format
msgid "%1$u minute before"
msgid_plural "%1$u minutes before"
msgstr[0] "Prieš %1$u minutę"
msgstr[1] "Prieš %1$u minutes"
msgstr[2] "Prieš %1$u minučių"

#: src/gui/event-editor/gcal-alarm-row.c:199
msgid "Event start time"
msgstr "Įvykio pradžios laikas"

#: src/gui/event-editor/gcal-alarm-row.ui:18
msgid "Toggles the sound of the alarm"
msgstr "Perjungia žadintuvo garsą"

#: src/gui/event-editor/gcal-alarm-row.ui:35
msgid "Remove the alarm"
msgstr "Pašalinti žadintuvą"

#: src/gui/event-editor/gcal-event-editor-dialog.c:201
msgid "Save"
msgstr "Įrašyti"

#: src/gui/event-editor/gcal-event-editor-dialog.c:201
#: src/gui/event-editor/gcal-event-editor-dialog.ui:108
msgid "Done"
msgstr "Atlikta"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:37
msgid "Click to select the calendar"
msgstr "Spustelėkite kalendoriui pasirinkti"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:157
msgid "Schedule"
msgstr "Tvarkaraštis"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:178
msgid "Reminders"
msgstr "Priminimai"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:201
msgid "Notes"
msgstr "Užrašai"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:226
msgid "Delete Event"
msgstr "Pašalinti įvykį"

#: src/gui/event-editor/gcal-reminders-section.ui:28
msgid "Add a Reminder…"
msgstr "Pridėti priminimą…"

#: src/gui/event-editor/gcal-reminders-section.ui:55
msgid "5 minutes"
msgstr "5 minutės"

#: src/gui/event-editor/gcal-reminders-section.ui:64
msgid "10 minutes"
msgstr "10 minučių"

#: src/gui/event-editor/gcal-reminders-section.ui:73
#| msgid "5 minutes"
msgid "15 minutes"
msgstr "15 minučių"

#: src/gui/event-editor/gcal-reminders-section.ui:82
msgid "30 minutes"
msgstr "30 miniučių"

#: src/gui/event-editor/gcal-reminders-section.ui:91
msgid "1 hour"
msgstr "1 valanda"

#: src/gui/event-editor/gcal-reminders-section.ui:99
msgid "1 day"
msgstr "1 diena"

#: src/gui/event-editor/gcal-reminders-section.ui:108
msgid "2 days"
msgstr "2 dienos"

#: src/gui/event-editor/gcal-reminders-section.ui:117
msgid "3 days"
msgstr "3 dienos"

#: src/gui/event-editor/gcal-reminders-section.ui:126
msgid "1 week"
msgstr "1 savaitė"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:241
#, c-format
msgid "Last %A"
msgstr "Paskutinį %A"

#: src/gui/event-editor/gcal-schedule-section.c:245
msgid "Yesterday"
msgstr "Vakar"

#: src/gui/event-editor/gcal-schedule-section.c:249 src/gui/gcal-window.ui:187
#: src/gui/views/gcal-year-view.c:282 src/gui/views/gcal-year-view.c:560
#: src/gui/views/gcal-year-view.ui:88
msgid "Today"
msgstr "Šiandien"

#: src/gui/event-editor/gcal-schedule-section.c:253
msgid "Tomorrow"
msgstr "Rytoj"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:263
#, c-format
msgid "This %A"
msgstr "Šį %A"

#.
#. * Translators: %1$s is the formatted date (e.g. Today, Sunday, or even 2019-10-11) and %2$s is the
#. * formatted time (e.g. 03:14 PM, or 21:29)
#.
#: src/gui/event-editor/gcal-schedule-section.c:293
#, c-format
msgid "%1$s, %2$s"
msgstr "%1$s, %2$s"

#: src/gui/event-editor/gcal-schedule-section.ui:20
msgid "All Day"
msgstr "Visą dieną"

#: src/gui/event-editor/gcal-schedule-section.ui:40
msgid "Starts"
msgstr "Prasideda"

#: src/gui/event-editor/gcal-schedule-section.ui:90
msgid "Ends"
msgstr "Baigiasi"

#: src/gui/event-editor/gcal-schedule-section.ui:140
msgid "Repeat"
msgstr "Kartoti"

#: src/gui/event-editor/gcal-schedule-section.ui:150
msgid "No Repeat"
msgstr "Nekartoti"

#: src/gui/event-editor/gcal-schedule-section.ui:151
msgid "Daily"
msgstr "Kasdien"

#: src/gui/event-editor/gcal-schedule-section.ui:152
msgid "Monday – Friday"
msgstr "Pirmadienį – penktadienį"

#: src/gui/event-editor/gcal-schedule-section.ui:153
msgid "Weekly"
msgstr "Kas savaitę"

#: src/gui/event-editor/gcal-schedule-section.ui:154
msgid "Monthly"
msgstr "Kas mėnesį"

#: src/gui/event-editor/gcal-schedule-section.ui:155
msgid "Yearly"
msgstr "Kas metus"

#: src/gui/event-editor/gcal-schedule-section.ui:168
msgid "End Repeat"
msgstr "Baigti pasikartojimą"

#: src/gui/event-editor/gcal-schedule-section.ui:178
msgid "Forever"
msgstr "Neribotai"

#: src/gui/event-editor/gcal-schedule-section.ui:179
msgid "No. of occurrences"
msgstr "Pasikartojimų skaičius"

#: src/gui/event-editor/gcal-schedule-section.ui:180
msgid "Until Date"
msgstr "Iki datos"

#: src/gui/event-editor/gcal-schedule-section.ui:193
msgid "Number of Occurrences"
msgstr "Pasikartojimų skaičius"

#: src/gui/event-editor/gcal-schedule-section.ui:213
msgid "End Repeat Date"
msgstr "Pasikartojimų pabaigos data"

#: src/gui/event-editor/gcal-summary-section.c:78
#: src/gui/gcal-quick-add-popover.c:689
msgid "Unnamed event"
msgstr "Bevardis įvykis"

#: src/gui/event-editor/gcal-summary-section.ui:19
msgid "Title"
msgstr "Pavadinimas"

#: src/gui/event-editor/gcal-time-selector.ui:22
msgid ":"
msgstr ":"

#: src/gui/event-editor/gcal-time-selector.ui:47
#: src/gui/views/gcal-week-view.c:440
msgid "AM"
msgstr "AM"

#: src/gui/event-editor/gcal-time-selector.ui:48
#: src/gui/views/gcal-week-view.c:440
msgid "PM"
msgstr "PM"

#: src/gui/gcal-application.c:63
msgid "Quit GNOME Calendar"
msgstr "Išeiti iš GNOME kalendoriaus"

#: src/gui/gcal-application.c:68
msgid "Display version number"
msgstr "Rodyti versijos numerį"

#: src/gui/gcal-application.c:73
msgid "Enable debug messages"
msgstr "Įjungti derinimo pranešimus"

#: src/gui/gcal-application.c:78
msgid "Open calendar on the passed date"
msgstr "Atverti kalendorių praėjusia data"

#: src/gui/gcal-application.c:83
msgid "Open calendar showing the passed event"
msgstr "Atverti kalendorių parodant praėjusį įvykį"

#: src/gui/gcal-application.c:234
#, c-format
msgid "Copyright © 2012–%d The Calendar authors"
msgstr "Autorių teisės saugomos © 2012–%d Kalendoriaus kūrėjai"

#: src/gui/gcal-application.c:288
msgid "translator-credits"
msgstr ""
"Išvertė:\n"
"Jonas Skarulskis <jonas.ska@gmail.com>"

#: src/gui/gcal-calendar-popover.ui:43
msgid "_Synchronize Calendars"
msgstr "_Sinchronizuoti kalendorius"

#: src/gui/gcal-calendar-popover.ui:51
msgid "Manage Calendars…"
msgstr "Tvarkyti kalendorius…"

#: src/gui/gcal-calendar-popover.ui:75
msgctxt "tooltip"
msgid "Synchronizing remote calendars…"
msgstr "Sinchronizuojami nuotoliniai kalendoriai…"

#. Translators: %s is the location of the event (e.g. "Downtown, 3rd Avenue")
#: src/gui/gcal-event-widget.c:428
#, c-format
msgid "At %s"
msgstr "Ties %s"

#: src/gui/gcal-quick-add-popover.c:117
#, c-format
msgid "%s (this calendar is read-only)"
msgstr "%s (šis kalendorius yra tik skaitymui)"

#: src/gui/gcal-quick-add-popover.c:244
msgid "from next Monday"
msgstr "nuo kito pirmadienio"

#: src/gui/gcal-quick-add-popover.c:245
msgid "from next Tuesday"
msgstr "nuo kito antradienio"

#: src/gui/gcal-quick-add-popover.c:246
msgid "from next Wednesday"
msgstr "nuo kito trečiadienio"

#: src/gui/gcal-quick-add-popover.c:247
msgid "from next Thursday"
msgstr "nuo kito ketvirtadienio"

#: src/gui/gcal-quick-add-popover.c:248
msgid "from next Friday"
msgstr "nuo kito penktadienio"

#: src/gui/gcal-quick-add-popover.c:249
msgid "from next Saturday"
msgstr "nuo kito šeštadienio"

#: src/gui/gcal-quick-add-popover.c:250
msgid "from next Sunday"
msgstr "nuo kito sekmadienio"

#: src/gui/gcal-quick-add-popover.c:255
msgid "to next Monday"
msgstr "iki kito pirmadienio"

#: src/gui/gcal-quick-add-popover.c:256
msgid "to next Tuesday"
msgstr "iki kito antradienio"

#: src/gui/gcal-quick-add-popover.c:257
msgid "to next Wednesday"
msgstr "iki kito trečiadienio"

#: src/gui/gcal-quick-add-popover.c:258
msgid "to next Thursday"
msgstr "iki kito ketvirtadienio"

#: src/gui/gcal-quick-add-popover.c:259
msgid "to next Friday"
msgstr "iki kito penktadienio"

#: src/gui/gcal-quick-add-popover.c:260
msgid "to next Saturday"
msgstr "iki kito šeštadienio"

#: src/gui/gcal-quick-add-popover.c:261
msgid "to next Sunday"
msgstr "iki kito sekmadienio"

#: src/gui/gcal-quick-add-popover.c:266
msgid "January"
msgstr "Sausis"

#: src/gui/gcal-quick-add-popover.c:267
msgid "February"
msgstr "Vasaris"

#: src/gui/gcal-quick-add-popover.c:268
msgid "March"
msgstr "Kovas"

#: src/gui/gcal-quick-add-popover.c:269
msgid "April"
msgstr "Balandis"

#: src/gui/gcal-quick-add-popover.c:270
msgid "May"
msgstr "Gegužė"

#: src/gui/gcal-quick-add-popover.c:271
msgid "June"
msgstr "Birželis"

#: src/gui/gcal-quick-add-popover.c:272
msgid "July"
msgstr "Liepa"

#: src/gui/gcal-quick-add-popover.c:273
msgid "August"
msgstr "Rugpjūtis"

#: src/gui/gcal-quick-add-popover.c:274
msgid "September"
msgstr "Rugsėjis"

#: src/gui/gcal-quick-add-popover.c:275
msgid "October"
msgstr "Spalis"

#: src/gui/gcal-quick-add-popover.c:276
msgid "November"
msgstr "Lapkritis"

#: src/gui/gcal-quick-add-popover.c:277
msgid "December"
msgstr "Gruodis"

#: src/gui/gcal-quick-add-popover.c:286
#, c-format
msgid "from Today"
msgstr "nuo šiandien"

#: src/gui/gcal-quick-add-popover.c:290
#, c-format
msgid "from Tomorrow"
msgstr "nuo rytojaus"

#: src/gui/gcal-quick-add-popover.c:294
#, c-format
msgid "from Yesterday"
msgstr "nuo vakar"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:312
#, c-format
msgid "from %1$s %2$s"
msgstr "nuo %1$s %2$s"

#: src/gui/gcal-quick-add-popover.c:323
#, c-format
msgid "to Today"
msgstr "iki šiandien"

#: src/gui/gcal-quick-add-popover.c:327
#, c-format
msgid "to Tomorrow"
msgstr "iki rytojaus"

#: src/gui/gcal-quick-add-popover.c:331
#, c-format
msgid "to Yesterday"
msgstr "iki vakar"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:349
#, c-format
msgid "to %1$s %2$s"
msgstr "iki %1$s %2$s"

#. Translators: %1$s is the start date (e.g. "from Today") and %2$s is the end date (e.g. "to Tomorrow")
#: src/gui/gcal-quick-add-popover.c:356
#, c-format
msgid "New Event %1$s %2$s"
msgstr "Naujas įvykis %1$s %2$s"

#: src/gui/gcal-quick-add-popover.c:373
#, c-format
msgid "New Event Today"
msgstr "Naujas įvykis šiandien"

#: src/gui/gcal-quick-add-popover.c:377
#, c-format
msgid "New Event Tomorrow"
msgstr "Naujas įvykis rytoj"

#: src/gui/gcal-quick-add-popover.c:381
#, c-format
msgid "New Event Yesterday"
msgstr "Naujas įvykis vakar"

#: src/gui/gcal-quick-add-popover.c:387
msgid "New Event next Monday"
msgstr "Naujas įvykis kitą pirmadienį"

#: src/gui/gcal-quick-add-popover.c:388
msgid "New Event next Tuesday"
msgstr "Naujas įvykis kitą antradienį"

#: src/gui/gcal-quick-add-popover.c:389
msgid "New Event next Wednesday"
msgstr "Naujas įvykis kitą trečiadienį"

#: src/gui/gcal-quick-add-popover.c:390
msgid "New Event next Thursday"
msgstr "Naujas įvykis kitą ketvirtadienį"

#: src/gui/gcal-quick-add-popover.c:391
msgid "New Event next Friday"
msgstr "Naujas įvykis kitą penktadienį"

#: src/gui/gcal-quick-add-popover.c:392
msgid "New Event next Saturday"
msgstr "Naujas įvykis kitą šeštadienį"

#: src/gui/gcal-quick-add-popover.c:393
msgid "New Event next Sunday"
msgstr "Naujas įvykis kitą sekmadienį"

#. Translators: %d is the numeric day of month
#: src/gui/gcal-quick-add-popover.c:405
#, c-format
msgid "New Event on January %d"
msgstr "Naujas įvykis sausio %d"

#: src/gui/gcal-quick-add-popover.c:406
#, c-format
msgid "New Event on February %d"
msgstr "Naujas įvykis vasario %d"

#: src/gui/gcal-quick-add-popover.c:407
#, c-format
msgid "New Event on March %d"
msgstr "Naujas įvykis kovo %d"

#: src/gui/gcal-quick-add-popover.c:408
#, c-format
msgid "New Event on April %d"
msgstr "Naujas įvykis balandžio %d"

#: src/gui/gcal-quick-add-popover.c:409
#, c-format
msgid "New Event on May %d"
msgstr "Naujas įvykis gegužės %d"

#: src/gui/gcal-quick-add-popover.c:410
#, c-format
msgid "New Event on June %d"
msgstr "Naujas įvykis birželio %d"

#: src/gui/gcal-quick-add-popover.c:411
#, c-format
msgid "New Event on July %d"
msgstr "Naujas įvykis liepos %d"

#: src/gui/gcal-quick-add-popover.c:412
#, c-format
msgid "New Event on August %d"
msgstr "Naujas įvykis rugpjūčio %d"

#: src/gui/gcal-quick-add-popover.c:413
#, c-format
msgid "New Event on September %d"
msgstr "Naujas įvykis rugsėjo %d"

#: src/gui/gcal-quick-add-popover.c:414
#, c-format
msgid "New Event on October %d"
msgstr "Naujas įvykis spalio %d"

#: src/gui/gcal-quick-add-popover.c:415
#, c-format
msgid "New Event on November %d"
msgstr "Naujas įvykis lapkričio %d"

#: src/gui/gcal-quick-add-popover.c:416
#, c-format
msgid "New Event on December %d"
msgstr "Naujas įvykis gruodžio %d"

#: src/gui/gcal-quick-add-popover.ui:117
msgid "Edit Details…"
msgstr "Keisti informaciją..."

#: src/gui/gcal-weather-settings.ui:12 src/gui/gcal-window.ui:327
msgid "_Weather"
msgstr "_Oras"

#: src/gui/gcal-weather-settings.ui:30
msgid "Show Weather"
msgstr "Rodyti orus"

#: src/gui/gcal-weather-settings.ui:53
msgid "Automatic Location"
msgstr "Automatinė vieta"

#: src/gui/gcal-window.c:672
msgid "Another event deleted"
msgstr "Dar vienas įvykis ištrintas"

#: src/gui/gcal-window.c:676
msgid "Event deleted"
msgstr "Įvykis ištrintas"

#: src/gui/gcal-window.ui:47
msgid "Week"
msgstr "Savaitė"

#: src/gui/gcal-window.ui:62
msgid "Month"
msgstr "Mėnuo"

#: src/gui/gcal-window.ui:76
msgid "Year"
msgstr "Metai"

#: src/gui/gcal-window.ui:176
msgctxt "tooltip"
msgid "Add a new event"
msgstr "Pridėti naujas įvykį"

#: src/gui/gcal-window.ui:265
msgid "Manage your calendars"
msgstr "Tvarkykite savo kalendorius"

#: src/gui/gcal-window.ui:276
msgctxt "tooltip"
msgid "Search for events"
msgstr "Ieškoti įvykių"

#: src/gui/gcal-window.ui:318
msgid "_Online Accounts…"
msgstr "_Internetinės paskyros…"

#: src/gui/gcal-window.ui:342
msgid "_Keyboard Shortcuts"
msgstr "_Klaviatūros trumpiniai"

#: src/gui/gcal-window.ui:351
msgid "_About Calendar"
msgstr "_Apie kalendorių"

#: src/gui/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Bendrieji"

#: src/gui/gtk/help-overlay.ui:17
msgctxt "shortcut window"
msgid "New event"
msgstr "Naujas įvykis"

#: src/gui/gtk/help-overlay.ui:24
msgctxt "shortcut window"
msgid "Close window"
msgstr "Užverti langą"

#: src/gui/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Search"
msgstr "Ieškoti"

#: src/gui/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Show help"
msgstr "Rodyti žinyną"

#: src/gui/gtk/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "Trumpiniai"

#: src/gui/gtk/help-overlay.ui:54
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Naršymas"

#: src/gui/gtk/help-overlay.ui:58
msgctxt "shortcut window"
msgid "Go back"
msgstr "Grįžti atgal"

#: src/gui/gtk/help-overlay.ui:65
msgctxt "shortcut window"
msgid "Go forward"
msgstr "Eiti pirmyn"

#: src/gui/gtk/help-overlay.ui:72
msgctxt "shortcut window"
msgid "Show today"
msgstr "Rodyti šiandieną"

#: src/gui/gtk/help-overlay.ui:79
msgctxt "shortcut window"
msgid "Next view"
msgstr "Kitas rodinys"

#: src/gui/gtk/help-overlay.ui:86
msgctxt "shortcut window"
msgid "Previous view"
msgstr "Ankstesnis rodinys"

#: src/gui/gtk/help-overlay.ui:95
msgctxt "shortcut window"
msgid "View"
msgstr "Rodinys"

#: src/gui/gtk/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Week view"
msgstr "Savaitės rodinys"

#: src/gui/gtk/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Month view"
msgstr "Mėnėsio rodinys"

#: src/gui/gtk/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Year view"
msgstr "Metų rodinys"

#: src/gui/views/gcal-month-popover.ui:91
msgid "New Event…"
msgstr "Naujas įvykis…"

#: src/gui/views/gcal-week-grid.c:681 src/gui/views/gcal-week-view.c:293
msgid "00 AM"
msgstr "00 AM"

#: src/gui/views/gcal-week-grid.c:684 src/gui/views/gcal-week-view.c:296
msgid "00:00"
msgstr "00:00"

#: src/gui/views/gcal-week-header.c:465
#, c-format
msgid "Other event"
msgid_plural "Other %d events"
msgstr[0] "Kitas %d įvykis"
msgstr[1] "Kiti %d įvykiai"
msgstr[2] "Kitų %d įvykių"

#: src/gui/views/gcal-week-header.c:1004
#, c-format
msgid "week %d"
msgstr "%d savaitė"

#. Translators: This is a date format in the sidebar of the year
#. * view when the selection starts at the specified day and the
#. * end is unspecified.
#: src/gui/views/gcal-year-view.c:291
msgid "%B %d…"
msgstr "%B %d…"

#. Translators: This is a date format in the sidebar of the year
#. * view when there is only one specified day selected.
#. Translators: This is a date format in the sidebar of the year view.
#: src/gui/views/gcal-year-view.c:297 src/gui/views/gcal-year-view.c:563
msgid "%B %d"
msgstr "%B %d"

#: src/gui/views/gcal-year-view.ui:133
msgid "No events"
msgstr "Įvykių nėra"

#: src/gui/views/gcal-year-view.ui:155
msgid "Add Event…"
msgstr "Pridėti įvykį..."

#: src/utils/gcal-utils.c:958
msgid ""
"The event you are trying to modify is recurring. The changes you have "
"selected should be applied to:"
msgstr ""
"Keičiamas įvykis yra pasikartojantis. Jūsų pasirinkti pakeitimai turi būti "
"taikomi:"

#: src/utils/gcal-utils.c:961
msgid "_Cancel"
msgstr "_Atsisakyti"

#: src/utils/gcal-utils.c:963
msgid "_Only This Event"
msgstr "_Tik šiam įvykiui"

#: src/utils/gcal-utils.c:970
msgid "_Subsequent events"
msgstr "Vė_lesniems įvykimas"

#: src/utils/gcal-utils.c:972
msgid "_All events"
msgstr "_Visiems įvykiams"

#~ msgid "Check this out!"
#~ msgstr "Išbandykite!"

#~ msgid "Search for events"
#~ msgstr "Ieškoti įvykių"

#~ msgid "Calendar management"
#~ msgstr "Kalendoriaus tvarkymas"

#~ msgid "Date"
#~ msgstr "Data"

#~ msgid "Time"
#~ msgstr "Laikas"

#~ msgid "From Web…"
#~ msgstr "Iš interneto…"

#~ msgid "New Local Calendar…"
#~ msgstr "Naujas vietinis kalendorius…"

#~ msgid "From File…"
#~ msgstr "Iš failo…"

#~ msgid "No results found"
#~ msgstr "Nieko nerasta"

#~ msgid "Try a different search"
#~ msgstr "Bandykite kitą paiešką"

#~ msgid "%d week before"
#~ msgid_plural "%d weeks before"
#~ msgstr[0] "Prieš %d savaitę"
#~ msgstr[1] "Prieš %d savaites"
#~ msgstr[2] "Prieš %d savaičių"

#~ msgid "%s AM"
#~ msgstr "%s AM"

#~ msgid "%s PM"
#~ msgstr "%s PM"

#~ msgid "org.gnome.Calendar"
#~ msgstr "org.gnome.Calendar"

#~ msgid "Open online account settings"
#~ msgstr "Atverti internetinių paskyrų nustatymus"

#~ msgid "Google"
#~ msgstr "Google"

#~ msgid "Click to set up"
#~ msgstr "Spauskite nustatymui"

#~ msgid "Nextcloud"
#~ msgstr "Nextcloud"

#~ msgid "Microsoft Exchange"
#~ msgstr "Microsoft Exchange"

#~ msgid "Overview"
#~ msgstr "Apžvalga"

#~ msgid "Edit Calendar"
#~ msgstr "Keisti kalendorių"

#~ msgid "Calendar Address"
#~ msgstr "Kalendoriaus adresas"

#~ msgid "Enter your credentials"
#~ msgstr "Įveskite savo įgaliojimus"

#~ msgid "All day"
#~ msgstr "Visą dieną"

#~ msgid "Use the entry above to search for events."
#~ msgstr "Pasinaudokite lauku viršuje įvykiams ieškoti."

#~ msgid "Select a calendar file"
#~ msgstr "Pasirinkite kalendoriaus failą"

#~ msgid "Open"
#~ msgstr "Atverti"

#~ msgid "Unnamed Calendar"
#~ msgstr "Bevardis kalendorius"

#~ msgid "Off"
#~ msgstr "Išjungta"

#~ msgid "On"
#~ msgstr "Įjungta"

#~ msgid "Expired"
#~ msgstr "Nebegalioja"

#~ msgid "_Calendars"
#~ msgstr "_Kalendoriai"

#~ msgid "_About"
#~ msgstr "_Apie"

#~ msgid "_Quit"
#~ msgstr "_Baigti"

#~ msgid "Add Eve_nt…"
#~ msgstr "Pri_dėti įvykį..."

#~| msgid "Add Eve_nt…"
#~ msgid "Add Eve_nt"
#~ msgstr "Pridėti į_vykį"

#~ msgid "Copyright © %d The Calendar authors"
#~ msgstr "Autorių teisės saugomos © %d Kalendoriaus kūrėjai"

#~ msgid "Other events"
#~ msgstr "Kiti įvykiai"

#~ msgid "— Calendar management"
#~ msgstr "— Kalendoriaus tvarkymas"

#~ msgid "New Event from %s to %s"
#~ msgstr "Naujas įvykis nuo %s iki %s"

#~ msgid "New Event on %s, %s – %s"
#~ msgstr "Naujas įvykis %s, %s – %s"

#~ msgid "List of the disabled sources"
#~ msgstr "Išjungtų šaltinių sąrašas"

#~ msgid "Sources disabled last time Calendar ran"
#~ msgstr "Šaltiniai, išjungti pastarąjį kartą naudojus kalendorių"

#~ msgid "_Search…"
#~ msgstr "_Ieškoti..."

#~ msgid "_Calendars…"
#~ msgstr "_Kalendoriai…"

#~ msgid "week %d / %d"
#~ msgstr "savaitė %d / %d"

#~ msgid "Midnight"
#~ msgstr "Vidurnaktis"

#~ msgid "Noon"
#~ msgstr "Vidurdienis"

#~ msgid "00:00 PM"
#~ msgstr "00:00 PM"

#~ msgid "Create"
#~ msgstr "Sukurti"

#~ msgid "%.2d:%.2d AM"
#~ msgstr "%.2d:%.2d AM"

#~ msgid "%.2d:%.2d PM"
#~ msgstr "%.2d:%.2d PM"

#~ msgid "Max Content Height"
#~ msgstr "Didžiausias turinio aukštis"

#~ msgid "The maximum height request that can be made."
#~ msgstr "Didžiausias aukštis, kokio galima prašyti."

#~ msgid "Max Content Width"
#~ msgstr "Didžiausias turinio plotis"

#~ msgid "The maximum width request that can be made."
#~ msgstr "Didžiausias plotis, kokio galima prašyti."

#~ msgid "%m/%d/%y"
#~ msgstr "%y-%m-%d"

#~ msgid "%.2d:%.2d %s"
#~ msgstr "%.2d:%.2d %s"
