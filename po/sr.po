# Serbian translation for gnome-calendar.
# Courtesy of Prevod.org team (http://prevod.org/) -- 2013—2017.
# Translators:
# This file is distributed under the same license as the gnome-calendar package.
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2013—2017.
# Милош Поповић <gpopac@gmail.com>, 2015.
# Марко М. Костић <marko.m.kostic@gmail.com>, 2016.
# Борисав Живановић <borisavzivanovic@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: gnome-calendar master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-calendar/issues\n"
"POT-Creation-Date: 2020-11-07 10:58+0000\n"
"PO-Revision-Date: 2021-01-15 01:58+0100\n"
"Last-Translator: Марко М. Костић <marko.m.kostic@gmail.com>\n"
"Language-Team: Serbian <(nothing)>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Project-Style: gnome\n"
"X-Generator: Poedit 2.4.2\n"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:7
#: data/org.gnome.Calendar.desktop.in.in:3 src/main.c:35
#: src/gui/gcal-application.c:277 src/gui/gcal-quick-add-popover.ui:187
#: src/gui/gcal-window.ui:170
msgid "Calendar"
msgstr "Календар"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:8
msgid "Calendar for GNOME"
msgstr "Календар за Гном"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:11
msgid ""
"GNOME Calendar is a simple and beautiful calendar application designed to "
"perfectly fit the GNOME desktop. By reusing the components which the GNOME "
"desktop is built on, Calendar nicely integrates with the GNOME ecosystem."
msgstr ""
"Гномов календар је једноставан и леп програм, осмишљен тако да се савршено "
"уклопи у Гномову радну површ. Користећи састојке на којима је изграђена "
"Гномова радна површ, Календар се одлично уклапа у Гномов екосистем."

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:16
msgid ""
"We aim to find the perfect balance between nicely crafted features and user-"
"centred usability. No excess, nothing missing. You’ll feel comfortable using "
"Calendar, like you’ve been using it for ages!"
msgstr ""
"Циљ нам је да пронађемо савршену равнотежу између лепо одрађених функција и "
"једноставности употребе тако да он не угрожава крајње кориснике. Без "
"претеривања, без недостатака. Осећаћете се удобно користећи Календар, као "
"што сте га већ годинама користили!"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:27
msgid "Week view"
msgstr "Недељни преглед"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:31
msgid "Year view"
msgstr "Годишњи преглед"

#: data/org.gnome.Calendar.desktop.in.in:4
msgid "Access and manage your calendars"
msgstr "Приступите и управљајте календаром"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calendar.desktop.in.in:13
msgid "Calendar;Event;Reminder;"
msgstr ""
"Calendar;Event;Reminder;Календар;Догађај;Подсетник;Kalendar;Događaj;"
"Podsetnik;Dogadjaj;"

#: data/org.gnome.calendar.gschema.xml.in:6
msgid "Window maximized"
msgstr "Увећан прозор"

#: data/org.gnome.calendar.gschema.xml.in:7
msgid "Window maximized state"
msgstr "Стање увећаног прозора"

#: data/org.gnome.calendar.gschema.xml.in:11
msgid "Window size"
msgstr "Величина прозора"

#: data/org.gnome.calendar.gschema.xml.in:12
msgid "Window size (width and height)."
msgstr "Величина прозора (ширина и висина)."

#: data/org.gnome.calendar.gschema.xml.in:16
msgid "Window position"
msgstr "Положај прозора"

#: data/org.gnome.calendar.gschema.xml.in:17
msgid "Window position (x and y)."
msgstr "Положај прозора (положено и усправно)."

#: data/org.gnome.calendar.gschema.xml.in:21
msgid "Type of the active view"
msgstr "Врста радног приказа"

#: data/org.gnome.calendar.gschema.xml.in:22
msgid "Type of the active window view, default value is: monthly view"
msgstr "Врста приказа радног прозора, подразумвано је: месечни приказ"

#: data/org.gnome.calendar.gschema.xml.in:26
msgid "Weather Service Configuration"
msgstr "Подешавање услуге временске прогнозе"

#: data/org.gnome.calendar.gschema.xml.in:27
msgid ""
"Whether weather reports are shown, automatic locations are used and a "
"location-name"
msgstr ""
"Да ли се временски извештаји приказују, да ли се аутоматска локација и назив "
"локације користе"

#: data/org.gnome.calendar.gschema.xml.in:31
msgid "Follow system night light"
msgstr "Прати ноћно светло система"

#: data/org.gnome.calendar.gschema.xml.in:32
msgid "Use GNOME night light setting to activate night-mode."
msgstr "Користи подешавање ноћног светла из Гнома за покретање ноћног режима."

#. Translators: %1$s is the start date and %2$s is the end date.
#: src/core/gcal-event.c:1882
#, c-format
msgid "%1$s — %2$s"
msgstr "%1$s — %2$s"

#.
#. * Translators: %1$s is the start date, %2$s is the start time,
#. * %3$s is the end date, and %4$s is the end time.
#.
#: src/core/gcal-event.c:1890
#, c-format
msgid "%1$s %2$s — %3$s %4$s"
msgstr "%1$s %2$s — %3$s %4$s"

#. Translators: %1$s is a date, %2$s is the start hour, and %3$s is the end hour
#. Translators: %1$s is the event name, %2$s is the start hour, and %3$s is the end hour
#: src/core/gcal-event.c:1906 src/gui/gcal-quick-add-popover.c:472
#, c-format
msgid "%1$s, %2$s – %3$s"
msgstr "%1$s, %2$s – %3$s"

#: src/gui/calendar-management/gcal-calendar-management-dialog.ui:42
msgid "Calendar Settings"
msgstr "Подешавање календара"

#: src/gui/calendar-management/gcal-calendars-page.c:359
msgid "Manage Calendars"
msgstr "Управљај календарима"

#. Update notification label
#: src/gui/calendar-management/gcal-calendars-page.c:389
#, c-format
msgid "Calendar <b>%s</b> removed"
msgstr "Календар <b>%s</b> је уклоњен"

#: src/gui/calendar-management/gcal-calendars-page.ui:31
#: src/gui/gcal-window.c:672 src/gui/gcal-window.c:676
msgid "Undo"
msgstr "Опозови"

#: src/gui/calendar-management/gcal-calendars-page.ui:97
msgid "Add Calendar…"
msgstr "Додај календар…"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:18
msgid "Account"
msgstr "Налог"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:43
msgid "Settings"
msgstr "Подешавања"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:64
#: src/gui/event-editor/gcal-summary-section.ui:37
msgid "Location"
msgstr "Место"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:93
msgid "Calendar name"
msgstr "Назив календара"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:121
#: src/gui/calendar-management/gcal-new-calendar-page.ui:65
msgid "Color"
msgstr "Боја"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:155
msgid "Display calendar"
msgstr "Прикажи календар"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:175
msgid "Add new events to this calendar by default"
msgstr "Подразумевано додај нове догађаје у овај календар"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:190
msgid "Remove Calendar"
msgstr "Уклони календар"

#: src/gui/calendar-management/gcal-new-calendar-page.c:505
msgid "New Calendar"
msgstr "Нови календар"

#: src/gui/calendar-management/gcal-new-calendar-page.c:667
msgid "Calendar files"
msgstr "Датотеке календара"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:33
#: src/gui/calendar-management/gcal-new-calendar-page.ui:41
msgid "Calendar Name"
msgstr "Назив календара"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:86
msgid "Import a Calendar"
msgstr "Увези календар"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:105
msgid ""
"Alternatively, enter the web address of an online calendar you want to "
"import, or open a supported calendar file."
msgstr ""
"Изборнио, унесите веб адресу мрежног календара који желите увести или "
"отворите подржану датотеку календара."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:137
msgid "Open a File"
msgstr "Отвори датотеку"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:158
msgid "Calendars"
msgstr "Календари"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:204
msgid ""
"If the calendar belongs to one of your online accounts, you can add it "
"through the <a href=\"GOA\">online account settings</a>."
msgstr ""
"Уколико календар припада једном од постојећих налога на мрежи, можете га "
"додати кроз подешавања <a href=\"GOA\">налога на мрежи</a>."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:240
msgid "User"
msgstr "Корисник"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:254
msgid "Password"
msgstr "Лозинка"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:302
#: src/gui/calendar-management/gcal-new-calendar-page.ui:341
#: src/gui/event-editor/gcal-event-editor-dialog.ui:17
msgid "Cancel"
msgstr "Откажи"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:311
msgid "Connect"
msgstr "Повежи се"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:348
#: src/gui/gcal-quick-add-popover.ui:130
msgid "Add"
msgstr "Додај"

#: src/gui/event-editor/gcal-alarm-row.c:85
#, c-format
msgid "%1$u day, %2$u hour, and %3$u minute before"
msgid_plural "%1$u day, %2$u hour, and %3$u minutes before"
msgstr[0] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[1] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[2] "пре %1$u дана, %2$u сати и %3$u минута"
msgstr[3] "пре једног дана, једног сата и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:89
#, c-format
msgid "%1$u day, %2$u hours, and %3$u minute before"
msgid_plural "%1$u day, %2$u hours, and %3$u minutes before"
msgstr[0] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[1] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[2] "пре %1$u дана, %2$u сати и %3$u минута"
msgstr[3] "пре једног дана, једног сата и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:95
#, c-format
msgid "%1$u days, %2$u hour, and %3$u minute before"
msgid_plural "%1$u days, %2$u hour, and %3$u minutes before"
msgstr[0] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[1] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[2] "пре %1$u дана, %2$u сати и %3$u минута"
msgstr[3] "пре једног дана, једног сата и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:99
#, c-format
msgid "%1$u days, %2$u hours, and %3$u minute before"
msgid_plural "%1$u days, %2$u hours, and %3$u minutes before"
msgstr[0] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[1] "пре %1$u дана, %2$u сата и %3$u минута"
msgstr[2] "пре %1$u дана, %2$u сати и %3$u минута"
msgstr[3] "пре једног дана, једног сата и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:114
#, c-format
msgid "%1$u day and %2$u hour before"
msgid_plural "%1$u day and %2$u hours before"
msgstr[0] "пре %1$u дана и %2$u сата"
msgstr[1] "пре %1$u дана и %2$u сата"
msgstr[2] "пре %1$u дана и %2$u сати"
msgstr[3] "пре једног дана и једног сата"

#: src/gui/event-editor/gcal-alarm-row.c:118
#, c-format
msgid "%1$u days and %2$u hour before"
msgid_plural "%1$u days and %2$u hours before"
msgstr[0] "пре %1$u дана и %2$u сата"
msgstr[1] "пре %1$u дана и %2$u сата"
msgstr[2] "пре %1$u дана и %2$u сати"
msgstr[3] "пре једног дана и једног сата"

#: src/gui/event-editor/gcal-alarm-row.c:134
#, c-format
msgid "%1$u day and %2$u minute before"
msgid_plural "%1$u day and %2$u minutes before"
msgstr[0] "пре %1$u дан и %2$u минут"
msgstr[1] "пре %1$u дана и %2$u минута"
msgstr[2] "пре %1$u дана и %2$u минута"
msgstr[3] "пре једног дана и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:138
#, c-format
msgid "%1$u days and %2$u minute before"
msgid_plural "%1$u days and %2$u minutes before"
msgstr[0] "пре %1$u дан и %2$u минут"
msgstr[1] "пре %1$u дана и %2$u минута"
msgstr[2] "пре %1$u дана и %2$u минута"
msgstr[3] "пре једног дана и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:148
#, c-format
msgid "%1$u day before"
msgid_plural "%1$u days before"
msgstr[0] "пре %1$u дан"
msgstr[1] "пре %1$u дана"
msgstr[2] "пре %1$u дана"
msgstr[3] "пре једног дана"

#: src/gui/event-editor/gcal-alarm-row.c:166
#, c-format
msgid "%1$u hour and %2$u minute before"
msgid_plural "%1$u hour and %2$u minutes before"
msgstr[0] "пре %1$u сат и %2$u минут"
msgstr[1] "пре %1$u сата и %2$u минута"
msgstr[2] "пре %1$u сати и %2$u минута"
msgstr[3] "пре једног сата и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:170
#, c-format
msgid "%1$u hours and %2$u minute before"
msgid_plural "%1$u hours and %2$u minutes before"
msgstr[0] "пре %1$u сат и %2$u минут"
msgstr[1] "пре %1$u сата и %2$u минута"
msgstr[2] "пре %1$u сати и %2$u минута"
msgstr[3] "пре једног сата и једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:180
#, c-format
msgid "%1$u hour before"
msgid_plural "%1$u hours before"
msgstr[0] "пре %1$u сат"
msgstr[1] "пре %1$u сата"
msgstr[2] "пре %1$u сати"
msgstr[3] "пре једног сата"

#: src/gui/event-editor/gcal-alarm-row.c:192
#, c-format
msgid "%1$u minute before"
msgid_plural "%1$u minutes before"
msgstr[0] "пре %1$u минут"
msgstr[1] "пре %1$u минута"
msgstr[2] "пре %1$u минута"
msgstr[3] "пре једног минута"

#: src/gui/event-editor/gcal-alarm-row.c:199
msgid "Event start time"
msgstr "Почетно време догађаја"

#: src/gui/event-editor/gcal-alarm-row.ui:18
msgid "Toggles the sound of the alarm"
msgstr "Прекидач за звук аларма"

#: src/gui/event-editor/gcal-alarm-row.ui:35
msgid "Remove the alarm"
msgstr "Уклони аларм"

#: src/gui/event-editor/gcal-event-editor-dialog.c:201
msgid "Save"
msgstr "Сачувај"

#: src/gui/event-editor/gcal-event-editor-dialog.c:201
#: src/gui/event-editor/gcal-event-editor-dialog.ui:108
msgid "Done"
msgstr "Урађено"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:37
msgid "Click to select the calendar"
msgstr "Кликните да изаберете календар"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:157
msgid "Schedule"
msgstr "Закажи"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:178
msgid "Reminders"
msgstr "Подсетници"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:201
msgid "Notes"
msgstr "Белешке"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:226
msgid "Delete Event"
msgstr "Обриши догађај"

#: src/gui/event-editor/gcal-reminders-section.ui:28
msgid "Add a Reminder…"
msgstr "Додај подсетник…"

#: src/gui/event-editor/gcal-reminders-section.ui:55
msgid "5 minutes"
msgstr "5 минута"

#: src/gui/event-editor/gcal-reminders-section.ui:64
msgid "10 minutes"
msgstr "10 минута"

#: src/gui/event-editor/gcal-reminders-section.ui:73
msgid "15 minutes"
msgstr "15 минута"

#: src/gui/event-editor/gcal-reminders-section.ui:82
msgid "30 minutes"
msgstr "30 минута"

#: src/gui/event-editor/gcal-reminders-section.ui:91
msgid "1 hour"
msgstr "1 сат"

#: src/gui/event-editor/gcal-reminders-section.ui:99
msgid "1 day"
msgstr "1 дан"

#: src/gui/event-editor/gcal-reminders-section.ui:108
msgid "2 days"
msgstr "2 дана"

#: src/gui/event-editor/gcal-reminders-section.ui:117
msgid "3 days"
msgstr "3 дана"

#: src/gui/event-editor/gcal-reminders-section.ui:126
msgid "1 week"
msgstr "1 недеља"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:241
#, c-format
msgid "Last %A"
msgstr "Последњи %A"

#: src/gui/event-editor/gcal-schedule-section.c:245
msgid "Yesterday"
msgstr "Јуче"

#: src/gui/event-editor/gcal-schedule-section.c:249 src/gui/gcal-window.ui:187
#: src/gui/views/gcal-year-view.c:282 src/gui/views/gcal-year-view.c:560
#: src/gui/views/gcal-year-view.ui:88
msgid "Today"
msgstr "Данас"

#: src/gui/event-editor/gcal-schedule-section.c:253
msgid "Tomorrow"
msgstr "Сутра"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:263
#, c-format
msgid "This %A"
msgstr "Овај %A"

#.
#. * Translators: %1$s is the formatted date (e.g. Today, Sunday, or even 2019-10-11) and %2$s is the
#. * formatted time (e.g. 03:14 PM, or 21:29)
#.
#: src/gui/event-editor/gcal-schedule-section.c:293
#, c-format
msgid "%1$s, %2$s"
msgstr "%1$s, %2$s"

#: src/gui/event-editor/gcal-schedule-section.ui:20
msgid "All Day"
msgstr "Читав дан"

#: src/gui/event-editor/gcal-schedule-section.ui:40
msgid "Starts"
msgstr "Почиње"

#: src/gui/event-editor/gcal-schedule-section.ui:90
msgid "Ends"
msgstr "Завршава"

#: src/gui/event-editor/gcal-schedule-section.ui:140
msgid "Repeat"
msgstr "Понови"

#: src/gui/event-editor/gcal-schedule-section.ui:150
msgid "No Repeat"
msgstr "Без понављања"

#: src/gui/event-editor/gcal-schedule-section.ui:151
msgid "Daily"
msgstr "Дневно"

#: src/gui/event-editor/gcal-schedule-section.ui:152
msgid "Monday – Friday"
msgstr "Понедељак – Петак"

#: src/gui/event-editor/gcal-schedule-section.ui:153
msgid "Weekly"
msgstr "Недељно"

#: src/gui/event-editor/gcal-schedule-section.ui:154
msgid "Monthly"
msgstr "Месечно"

#: src/gui/event-editor/gcal-schedule-section.ui:155
msgid "Yearly"
msgstr "Годишње"

#: src/gui/event-editor/gcal-schedule-section.ui:168
msgid "End Repeat"
msgstr "крај понављања"

#: src/gui/event-editor/gcal-schedule-section.ui:178
msgid "Forever"
msgstr "Заувек"

#: src/gui/event-editor/gcal-schedule-section.ui:179
msgid "No. of occurrences"
msgstr "Број понављања"

#: src/gui/event-editor/gcal-schedule-section.ui:180
msgid "Until Date"
msgstr "До датума"

#: src/gui/event-editor/gcal-schedule-section.ui:193
msgid "Number of Occurrences"
msgstr "Број понављања"

#: src/gui/event-editor/gcal-schedule-section.ui:213
msgid "End Repeat Date"
msgstr "Крајњи датум понављања"

#: src/gui/event-editor/gcal-summary-section.c:78
#: src/gui/gcal-quick-add-popover.c:689
msgid "Unnamed event"
msgstr "Неименовани догађај"

#: src/gui/event-editor/gcal-summary-section.ui:19
msgid "Title"
msgstr "Назив"

#: src/gui/event-editor/gcal-time-selector.ui:22
msgid ":"
msgstr ":"

#: src/gui/event-editor/gcal-time-selector.ui:47
#: src/gui/views/gcal-week-view.c:440
msgid "AM"
msgstr "преподне"

#: src/gui/event-editor/gcal-time-selector.ui:48
#: src/gui/views/gcal-week-view.c:440
msgid "PM"
msgstr "поподне"

#: src/gui/gcal-application.c:63
msgid "Quit GNOME Calendar"
msgstr "Изађи из Гномовог календара"

#: src/gui/gcal-application.c:68
msgid "Display version number"
msgstr "Приказује број издања"

#: src/gui/gcal-application.c:73
msgid "Enable debug messages"
msgstr "Омогући поруке за поправљање грешака"

#: src/gui/gcal-application.c:78
msgid "Open calendar on the passed date"
msgstr "Отвара календар за прошли датум"

#: src/gui/gcal-application.c:83
msgid "Open calendar showing the passed event"
msgstr "Отвара календар који приказује прошли догађај"

#: src/gui/gcal-application.c:234
#, c-format
msgid "Copyright © 2012–%d The Calendar authors"
msgstr "Ауторска права © 2012–%d аутори Календара"

#: src/gui/gcal-application.c:288
msgid "translator-credits"
msgstr ""
"Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Милош Поповић <gpopac@gmail.com>\n"
"Марко М. Костић <marko.m.kostic@gmail.com>\n"
"Борисав Живановић <borisavzivanovic@gmail.com>\n"
"\n"
"https://гном.срб — превод Гнома на српски језик"

#: src/gui/gcal-calendar-popover.ui:43
msgid "_Synchronize Calendars"
msgstr "_Усагласи календаре"

#: src/gui/gcal-calendar-popover.ui:51
msgid "Manage Calendars…"
msgstr "Управљај календарима…"

#: src/gui/gcal-calendar-popover.ui:75
msgctxt "tooltip"
msgid "Synchronizing remote calendars…"
msgstr "Усклађујем удаљене календаре…"

#. Translators: %s is the location of the event (e.g. "Downtown, 3rd Avenue")
#: src/gui/gcal-event-widget.c:428
#, c-format
msgid "At %s"
msgstr "У %s"

#: src/gui/gcal-quick-add-popover.c:117
#, c-format
msgid "%s (this calendar is read-only)"
msgstr "%s (овај календар је само за читање)"

#: src/gui/gcal-quick-add-popover.c:244
msgid "from next Monday"
msgstr "од следећег понедељка"

#: src/gui/gcal-quick-add-popover.c:245
msgid "from next Tuesday"
msgstr "од следећег уторка"

#: src/gui/gcal-quick-add-popover.c:246
msgid "from next Wednesday"
msgstr "од следеће среде"

#: src/gui/gcal-quick-add-popover.c:247
msgid "from next Thursday"
msgstr "од следећег четвртка"

#: src/gui/gcal-quick-add-popover.c:248
msgid "from next Friday"
msgstr "од следећег петка"

#: src/gui/gcal-quick-add-popover.c:249
msgid "from next Saturday"
msgstr "од следеће суботе"

#: src/gui/gcal-quick-add-popover.c:250
msgid "from next Sunday"
msgstr "од следеће недеље"

#: src/gui/gcal-quick-add-popover.c:255
msgid "to next Monday"
msgstr "до следећег понедељка"

#: src/gui/gcal-quick-add-popover.c:256
msgid "to next Tuesday"
msgstr "до следећег уторка"

#: src/gui/gcal-quick-add-popover.c:257
msgid "to next Wednesday"
msgstr "до следеће среде"

#: src/gui/gcal-quick-add-popover.c:258
msgid "to next Thursday"
msgstr "до следећег четвртка"

#: src/gui/gcal-quick-add-popover.c:259
msgid "to next Friday"
msgstr "до следећег петка"

#: src/gui/gcal-quick-add-popover.c:260
msgid "to next Saturday"
msgstr "до следеће суботе"

#: src/gui/gcal-quick-add-popover.c:261
msgid "to next Sunday"
msgstr "до следеће недеље"

#: src/gui/gcal-quick-add-popover.c:266
msgid "January"
msgstr "јануар"

#: src/gui/gcal-quick-add-popover.c:267
msgid "February"
msgstr "фебруар"

#: src/gui/gcal-quick-add-popover.c:268
msgid "March"
msgstr "март"

#: src/gui/gcal-quick-add-popover.c:269
msgid "April"
msgstr "април"

#: src/gui/gcal-quick-add-popover.c:270
msgid "May"
msgstr "мај"

#: src/gui/gcal-quick-add-popover.c:271
msgid "June"
msgstr "јун"

#: src/gui/gcal-quick-add-popover.c:272
msgid "July"
msgstr "јул"

#: src/gui/gcal-quick-add-popover.c:273
msgid "August"
msgstr "август"

#: src/gui/gcal-quick-add-popover.c:274
msgid "September"
msgstr "септембар"

#: src/gui/gcal-quick-add-popover.c:275
msgid "October"
msgstr "октобар"

#: src/gui/gcal-quick-add-popover.c:276
msgid "November"
msgstr "новембар"

#: src/gui/gcal-quick-add-popover.c:277
msgid "December"
msgstr "децембар"

#: src/gui/gcal-quick-add-popover.c:286
#, c-format
msgid "from Today"
msgstr "од данас"

#: src/gui/gcal-quick-add-popover.c:290
#, c-format
msgid "from Tomorrow"
msgstr "од сутра"

#: src/gui/gcal-quick-add-popover.c:294
#, c-format
msgid "from Yesterday"
msgstr "од јуче"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:312
#, c-format
msgid "from %1$s %2$s"
msgstr "од %1$s %2$s"

#: src/gui/gcal-quick-add-popover.c:323
#, c-format
msgid "to Today"
msgstr "до данас"

#: src/gui/gcal-quick-add-popover.c:327
#, c-format
msgid "to Tomorrow"
msgstr "до сутра"

#: src/gui/gcal-quick-add-popover.c:331
#, c-format
msgid "to Yesterday"
msgstr "до јуче"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:349
#, c-format
msgid "to %1$s %2$s"
msgstr "до %1$s %2$s"

#. Translators: %1$s is the start date (e.g. "from Today") and %2$s is the end date (e.g. "to Tomorrow")
#: src/gui/gcal-quick-add-popover.c:356
#, c-format
msgid "New Event %1$s %2$s"
msgstr "Нови догађај %1$s %2$s"

#: src/gui/gcal-quick-add-popover.c:373
#, c-format
msgid "New Event Today"
msgstr "Нови догађај данас"

#: src/gui/gcal-quick-add-popover.c:377
#, c-format
msgid "New Event Tomorrow"
msgstr "Нови догађај сутра"

#: src/gui/gcal-quick-add-popover.c:381
#, c-format
msgid "New Event Yesterday"
msgstr "Нови догађај јуче"

#: src/gui/gcal-quick-add-popover.c:387
msgid "New Event next Monday"
msgstr "Нови догађај следећег понедељка"

#: src/gui/gcal-quick-add-popover.c:388
msgid "New Event next Tuesday"
msgstr "Нови догађај следећег уторка"

#: src/gui/gcal-quick-add-popover.c:389
msgid "New Event next Wednesday"
msgstr "Нови догађај следеће среде"

#: src/gui/gcal-quick-add-popover.c:390
msgid "New Event next Thursday"
msgstr "Нови догађај следећег четвртка"

#: src/gui/gcal-quick-add-popover.c:391
msgid "New Event next Friday"
msgstr "Нови догађај следећег петка"

#: src/gui/gcal-quick-add-popover.c:392
msgid "New Event next Saturday"
msgstr "Нови догађај следеће суботе"

#: src/gui/gcal-quick-add-popover.c:393
msgid "New Event next Sunday"
msgstr "Нови догађај следеће недеље"

#. Translators: %d is the numeric day of month
#: src/gui/gcal-quick-add-popover.c:405
#, c-format
msgid "New Event on January %d"
msgstr "Нови догађај за %d. јануар"

#: src/gui/gcal-quick-add-popover.c:406
#, c-format
msgid "New Event on February %d"
msgstr "Нови догађај за %d. фебруар"

#: src/gui/gcal-quick-add-popover.c:407
#, c-format
msgid "New Event on March %d"
msgstr "Нови догађај за %d. март"

#: src/gui/gcal-quick-add-popover.c:408
#, c-format
msgid "New Event on April %d"
msgstr "Нови догађај за %d. април"

#: src/gui/gcal-quick-add-popover.c:409
#, c-format
msgid "New Event on May %d"
msgstr "Нови догађај за %d. мај"

#: src/gui/gcal-quick-add-popover.c:410
#, c-format
msgid "New Event on June %d"
msgstr "Нови догађај за %d. јун"

#: src/gui/gcal-quick-add-popover.c:411
#, c-format
msgid "New Event on July %d"
msgstr "Нови догађај за %d. јул"

#: src/gui/gcal-quick-add-popover.c:412
#, c-format
msgid "New Event on August %d"
msgstr "Нови догађај за %d. август"

#: src/gui/gcal-quick-add-popover.c:413
#, c-format
msgid "New Event on September %d"
msgstr "Нови догађај за %d. септембар"

#: src/gui/gcal-quick-add-popover.c:414
#, c-format
msgid "New Event on October %d"
msgstr "Нови догађај за %d. октобар"

#: src/gui/gcal-quick-add-popover.c:415
#, c-format
msgid "New Event on November %d"
msgstr "Нови догађај за %d. новембар"

#: src/gui/gcal-quick-add-popover.c:416
#, c-format
msgid "New Event on December %d"
msgstr "Нови догађај за %d. децембар"

#: src/gui/gcal-quick-add-popover.ui:117
msgid "Edit Details…"
msgstr "Уреди појединости…"

#: src/gui/gcal-weather-settings.ui:12 src/gui/gcal-window.ui:327
msgid "_Weather"
msgstr "_Време"

#: src/gui/gcal-weather-settings.ui:30
msgid "Show Weather"
msgstr "Прикажи прогнозу…"

#: src/gui/gcal-weather-settings.ui:53
msgid "Automatic Location"
msgstr "Аутоматска локација"

#: src/gui/gcal-window.c:672
msgid "Another event deleted"
msgstr "Други догађај је обрисан"

#: src/gui/gcal-window.c:676
msgid "Event deleted"
msgstr "Догађај је обрисан"

#: src/gui/gcal-window.ui:47
msgid "Week"
msgstr "Седмица"

#: src/gui/gcal-window.ui:62
msgid "Month"
msgstr "Месец"

#: src/gui/gcal-window.ui:76
msgid "Year"
msgstr "Година"

#: src/gui/gcal-window.ui:176
msgctxt "tooltip"
msgid "Add a new event"
msgstr "Додај нови догађај"

#: src/gui/gcal-window.ui:265
msgid "Manage your calendars"
msgstr "Управљајте вашим календарима"

#: src/gui/gcal-window.ui:276
msgctxt "tooltip"
msgid "Search for events"
msgstr "Тражи догађаје"

#: src/gui/gcal-window.ui:318
msgid "_Online Accounts…"
msgstr "Нал_ози на мрежи…"

#: src/gui/gcal-window.ui:342
msgid "_Keyboard Shortcuts"
msgstr "Пречице _тастатуре"

#: src/gui/gcal-window.ui:351
msgid "_About Calendar"
msgstr "_О Календару"

#: src/gui/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Опште"

#: src/gui/gtk/help-overlay.ui:17
msgctxt "shortcut window"
msgid "New event"
msgstr "Нови догађај"

#: src/gui/gtk/help-overlay.ui:24
msgctxt "shortcut window"
msgid "Close window"
msgstr "Затвори прозор"

#: src/gui/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Search"
msgstr "Претрага"

#: src/gui/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Show help"
msgstr "Прикажи помоћ"

#: src/gui/gtk/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "Пречице"

#: src/gui/gtk/help-overlay.ui:54
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Навигација"

#: src/gui/gtk/help-overlay.ui:58
msgctxt "shortcut window"
msgid "Go back"
msgstr "Иди назад"

#: src/gui/gtk/help-overlay.ui:65
msgctxt "shortcut window"
msgid "Go forward"
msgstr "Иди напред"

#: src/gui/gtk/help-overlay.ui:72
msgctxt "shortcut window"
msgid "Show today"
msgstr "Прикажи данашње"

#: src/gui/gtk/help-overlay.ui:79
msgctxt "shortcut window"
msgid "Next view"
msgstr "Следећи преглед"

#: src/gui/gtk/help-overlay.ui:86
msgctxt "shortcut window"
msgid "Previous view"
msgstr "Претходни преглед"

#: src/gui/gtk/help-overlay.ui:95
msgctxt "shortcut window"
msgid "View"
msgstr "Преглед"

#: src/gui/gtk/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Week view"
msgstr "Недељни преглед"

#: src/gui/gtk/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Month view"
msgstr "Месечни преглед"

#: src/gui/gtk/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Year view"
msgstr "Годишњи преглед"

#: src/gui/views/gcal-month-popover.ui:91
msgid "New Event…"
msgstr "Нови догађај…"

#: src/gui/views/gcal-week-grid.c:681 src/gui/views/gcal-week-view.c:293
msgid "00 AM"
msgstr "00 ПрП"

#: src/gui/views/gcal-week-grid.c:684 src/gui/views/gcal-week-view.c:296
msgid "00:00"
msgstr "00:00"

#: src/gui/views/gcal-week-header.c:465
#, c-format
msgid "Other event"
msgid_plural "Other %d events"
msgstr[0] "Други %d догађај"
msgstr[1] "Друга %d догађаја"
msgstr[2] "Других %d догађаја"
msgstr[3] "Други догађај"

#: src/gui/views/gcal-week-header.c:1004
#, c-format
msgid "week %d"
msgstr "недеља %d"

#. Translators: This is a date format in the sidebar of the year
#. * view when the selection starts at the specified day and the
#. * end is unspecified.
#: src/gui/views/gcal-year-view.c:291
msgid "%B %d…"
msgstr "%d. %B"

#. Translators: This is a date format in the sidebar of the year
#. * view when there is only one specified day selected.
#. Translators: This is a date format in the sidebar of the year view.
#: src/gui/views/gcal-year-view.c:297 src/gui/views/gcal-year-view.c:563
msgid "%B %d"
msgstr "%d. %B"

#: src/gui/views/gcal-year-view.ui:133
msgid "No events"
msgstr "Без догађаја"

#: src/gui/views/gcal-year-view.ui:155
msgid "Add Event…"
msgstr "Додај догађај…"

#: src/utils/gcal-utils.c:958
msgid ""
"The event you are trying to modify is recurring. The changes you have "
"selected should be applied to:"
msgstr ""
"Догађај који покушавате да измените се понавља. Измене коje желите да "
"направите се односе на:"

#: src/utils/gcal-utils.c:961
msgid "_Cancel"
msgstr "_Откажи"

#: src/utils/gcal-utils.c:963
msgid "_Only This Event"
msgstr "_Само за овај догађај"

#: src/utils/gcal-utils.c:970
msgid "_Subsequent events"
msgstr "_Пратећи догађаји"

#: src/utils/gcal-utils.c:972
msgid "_All events"
msgstr "Сви догађаји"

#~ msgid "Check this out!"
#~ msgstr "Погледај ово!"

#~ msgid "Search for events"
#~ msgstr "Тражи догађаје"

#~ msgid "Calendar management"
#~ msgstr "Управљање календаром"

#~ msgid "Date"
#~ msgstr "Датум"

#~ msgid "Time"
#~ msgstr "Време"

#~ msgid "From Web…"
#~ msgstr "Са веба…"

#~ msgid "New Local Calendar…"
#~ msgstr "Нови локални календар…"

#~ msgid "From File…"
#~ msgstr "Из датотеке…"

#~ msgid "No results found"
#~ msgstr "Нема резултата"

#~ msgid "Try a different search"
#~ msgstr "Покушајте другачију претрагу"

#~ msgid "%d week before"
#~ msgid_plural "%d weeks before"
#~ msgstr[0] "Пре %d недељу"
#~ msgstr[1] "Пре %d недеље"
#~ msgstr[2] "Пре %d недеља"
#~ msgstr[3] "Пре %d недељу"

#~ msgid "%s AM"
#~ msgstr "%s ПрП"

#~ msgid "%s PM"
#~ msgstr "%s ПоП"

#~ msgid "org.gnome.Calendar"
#~ msgstr "org.gnome.Calendar"

#~ msgid "Open online account settings"
#~ msgstr "Отвара подешавања налога на мрежи"

#~ msgid "Google"
#~ msgstr "Гугл"

#~ msgid "Click to set up"
#~ msgstr "Кликните за подешавање"

#~ msgid "Nextcloud"
#~ msgstr "мојОблак (Nextcloud)"

#~ msgid "Microsoft Exchange"
#~ msgstr "Мајкрософтова размена"

#~ msgid "Overview"
#~ msgstr "Преглед"

#~ msgid "Edit Calendar"
#~ msgstr "Уреди календар"

#~ msgid "Calendar Address"
#~ msgstr "Адреса календара"

#~ msgid "Enter your credentials"
#~ msgstr "Унесите ваше податке за приступ"

#~ msgid "All day"
#~ msgstr "Читав дан"

#~ msgid "Use the entry above to search for events."
#~ msgstr "Користите унос изнад да потражите догађаје."

#~ msgid "Select a calendar file"
#~ msgstr "Изаберите датотеку с календаром"

#~ msgid "Open"
#~ msgstr "Отвори"

#~ msgid "Unnamed Calendar"
#~ msgstr "Неименовани календар"

#~ msgid "Off"
#~ msgstr "Искљ."

#~ msgid "On"
#~ msgstr "Укљ."

#~ msgid "Expired"
#~ msgstr "Истекао"

#~ msgid "_Calendars"
#~ msgstr "_Календари"

#~ msgid "_About"
#~ msgstr "_О програму"

#~ msgid "_Quit"
#~ msgstr "_Изађи"

#~ msgid "Add Eve_nt…"
#~ msgstr "Додај _догађај…"

#~ msgid "Add Eve_nt"
#~ msgstr "Додај _догађај"

#~ msgid "Copyright © %d The Calendar authors"
#~ msgstr "Ауторска права © %d аутори Календара"

#~ msgid "Other events"
#~ msgstr "Други догађаји"

#~ msgid "— Calendar management"
#~ msgstr "— Управљање календаром"

#~ msgid "List of the disabled sources"
#~ msgstr "Списак искључених извора"

#~ msgid "Sources disabled last time Calendar ran"
#~ msgstr "Извори који су искључени приликом последњег покретања календара"

#~ msgid "_Search…"
#~ msgstr "_Претражи…"

#~ msgid "_Calendars…"
#~ msgstr "_Календари…"

#~ msgid "week %d / %d"
#~ msgstr "недеља %d од %d"

#~ msgid "New Event from %s to %s"
#~ msgstr "Нови догађај од %s до %s"

#~ msgid "New Event on %s, %s – %s"
#~ msgstr "Нови догађај %s, %s – %s"

#~ msgid "Midnight"
#~ msgstr "Поноћ"

#~ msgid "Noon"
#~ msgstr "Подне"

#~ msgid "00:00 PM"
#~ msgstr "00:00"

#~ msgid "Create"
#~ msgstr "Направи"

#~ msgid "%.2d:%.2d AM"
#~ msgstr "%.2d:%.2d AM"

#~ msgid "%.2d:%.2d PM"
#~ msgstr "%.2d:%.2d PM"

#~ msgid "%m/%d/%y"
#~ msgstr "%d.%m.%y."

#~ msgid "More Details"
#~ msgstr "Више појединости"

#~| msgid "%.2d:%.2d AM"
#~ msgid "%.2d:%.2d %s"
#~ msgstr "%.2d:%.2d %s"

#~ msgid "Results for \"%s\""
#~ msgstr "Резултати за „%s“"

#~ msgid "Unable to initialize GtkClutter"
#~ msgstr "Не могу да покренем Гтк Галамџију"

#~ msgid "Weeks"
#~ msgstr "Недеље"

#~ msgid "Months"
#~ msgstr "Месеци"

#~ msgid "Years"
#~ msgstr "Године"

#~ msgid "Change the date"
#~ msgstr "Измените датум"

#~ msgid "What (e.g. Alien Invasion)"
#~ msgstr "Шта (нпр. Напад свемираца)"

#~ msgid "Email"
#~ msgstr "Ел. пошта"

#~ msgid "WWW 99 - WWW 99"
#~ msgstr "ШШШ 99 — ШШШ 99"

#~ msgid "Change the time"
#~ msgstr "Измените време"

#~ msgid "List"
#~ msgstr "Списак"

#~ msgid "Back"
#~ msgstr "Назад"

#~ msgid "Edit"
#~ msgstr "Уреди"

#~ msgid "Year %d"
#~ msgstr "%d. година"
